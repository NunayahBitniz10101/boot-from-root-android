package p002eu.chainfire.p003a.p004a;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;
import p002eu.chainfire.librootjava.C0078d;

/* renamed from: eu.chainfire.a.a.e */
public class C0027e {

    /* renamed from: a */
    protected final ReentrantLock f109a = new ReentrantLock(true);

    /* renamed from: b */
    private ArrayList<Integer> f110b = new ArrayList<>();

    /* renamed from: c */
    private void m90c() {
        this.f109a.lock();
        try {
            Iterator<Integer> it = this.f110b.iterator();
            while (it.hasNext()) {
                GLES20.glDeleteTextures(1, new int[]{it.next().intValue()}, 0);
            }
            this.f110b.clear();
        } finally {
            this.f109a.unlock();
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.a.a.f.a(java.lang.String):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r0 = new int[1];
        android.opengl.GLES20.glGenTextures(1, r0, 0);
        p002eu.chainfire.p003a.p004a.C0028f.m99a((java.lang.String) "glGenTextures");
        r0 = r0[0];
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int mo95a() {
        /*
            r4 = this;
            java.util.concurrent.locks.ReentrantLock r0 = r4.f109a
            r0.lock()
        L_0x0005:
            java.util.ArrayList<java.lang.Integer> r0 = r4.f110b     // Catch:{ all -> 0x0047 }
            int r0 = r0.size()     // Catch:{ all -> 0x0047 }
            r1 = 1
            if (r0 <= 0) goto L_0x0039
            java.util.ArrayList<java.lang.Integer> r0 = r4.f110b     // Catch:{ all -> 0x0047 }
            java.util.ArrayList<java.lang.Integer> r2 = r4.f110b     // Catch:{ all -> 0x0047 }
            int r2 = r2.size()     // Catch:{ all -> 0x0047 }
            int r2 = r2 - r1
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0047 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x0047 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x0047 }
            java.util.ArrayList<java.lang.Integer> r2 = r4.f110b     // Catch:{ all -> 0x0047 }
            java.util.ArrayList<java.lang.Integer> r3 = r4.f110b     // Catch:{ all -> 0x0047 }
            int r3 = r3.size()     // Catch:{ all -> 0x0047 }
            int r3 = r3 - r1
            r2.remove(r3)     // Catch:{ all -> 0x0047 }
            boolean r1 = android.opengl.GLES20.glIsTexture(r0)     // Catch:{ all -> 0x0047 }
            if (r1 == 0) goto L_0x0005
        L_0x0033:
            java.util.concurrent.locks.ReentrantLock r1 = r4.f109a
            r1.unlock()
            return r0
        L_0x0039:
            int[] r0 = new int[r1]     // Catch:{ all -> 0x0047 }
            r2 = 0
            android.opengl.GLES20.glGenTextures(r1, r0, r2)     // Catch:{ all -> 0x0047 }
            java.lang.String r1 = "glGenTextures"
            p002eu.chainfire.p003a.p004a.C0028f.m99a(r1)     // Catch:{ all -> 0x0047 }
            r0 = r0[r2]     // Catch:{ all -> 0x0047 }
            goto L_0x0033
        L_0x0047:
            r0 = move-exception
            java.util.concurrent.locks.ReentrantLock r1 = r4.f109a
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p003a.p004a.C0027e.mo95a():int");
    }

    /* JADX INFO: used method not loaded: eu.chainfire.a.a.f.a(java.lang.String):null, types can be incorrect */
    /* renamed from: a */
    public int mo96a(Bitmap bitmap) {
        int[] iArr = {mo95a()};
        if (iArr[0] != 0) {
            GLES20.glBindTexture(3553, iArr[0]);
            GLES20.glTexParameteri(3553, 10242, 33071);
            GLES20.glTexParameteri(3553, 10243, 33071);
            GLES20.glTexParameteri(3553, 10241, 9729);
            GLES20.glTexParameteri(3553, 10240, 9729);
            GLUtils.texImage2D(3553, 0, bitmap, 0);
            C0028f.m99a((String) "texImage2D");
        }
        if (iArr[0] == 0) {
            C0078d.m290a("Error loading texture (empty texture handle)", new Object[0]);
        }
        if (!GLES20.glIsTexture(iArr[0])) {
            C0078d.m290a("Error loading texture (invalid texture handle)", new Object[0]);
        }
        return iArr[0];
    }

    /* renamed from: a */
    public void mo97a(int i) {
        this.f109a.lock();
        try {
            GLES20.glDeleteTextures(1, new int[]{i}, 0);
            this.f110b.add(Integer.valueOf(i));
        } finally {
            this.f109a.unlock();
        }
    }

    /* renamed from: b */
    public void mo98b() {
        m90c();
    }
}
