package p002eu.chainfire.p003a;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.display.DisplayManager;
import android.opengl.EGL14;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.view.Surface;
import java.lang.reflect.Method;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.librootjava.C0083h;

/* renamed from: eu.chainfire.a.a */
public abstract class C0012a {
    public static final int reservedArgs = 1;

    /* renamed from: a */
    private volatile boolean f33a = true;

    /* renamed from: b */
    private volatile boolean f34b = false;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public volatile int f35c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public volatile int f36d = 0;

    /* renamed from: e */
    private volatile long f37e = 0;

    /* renamed from: f */
    private volatile String f38f = null;

    /* renamed from: g */
    private volatile Context f39g = null;

    /* renamed from: h */
    private volatile DisplayManager f40h = null;

    /* renamed from: i */
    private volatile int f41i = 0;

    /* renamed from: j */
    private Object f42j = null;

    /* renamed from: k */
    private Class<?> f43k = null;

    /* renamed from: l */
    private Object f44l = null;

    /* renamed from: m */
    private Method f45m = null;

    /* renamed from: n */
    private Method f46n = null;

    /* renamed from: o */
    private Method f47o = null;

    /* renamed from: p */
    private Method f48p = null;

    /* renamed from: q */
    private Method f49q = null;

    /* renamed from: r */
    private Method f50r = null;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public Surface f51s = null;

    /* renamed from: eu.chainfire.a.a$a */
    public interface C0015a {
        /* renamed from: a */
        void mo72a(Canvas canvas);
    }

    /* renamed from: eu.chainfire.a.a$b */
    public interface C0016b {
        void onGLRenderFrame();
    }

    /* renamed from: eu.chainfire.a.a$c */
    public interface C0017c {
        /* renamed from: a */
        void mo74a(Canvas canvas);
    }

    /* renamed from: eu.chainfire.a.a$d */
    public interface C0018d {
        /* renamed from: a */
        void mo75a(Surface surface);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002c A[Catch:{ Exception -> 0x0044 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0038 A[Catch:{ Exception -> 0x0044 }] */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean m45f() {
        /*
            r6 = this;
            r0 = 0
            android.hardware.display.DisplayManager r1 = r6.f40h     // Catch:{ Exception -> 0x0044 }
            if (r1 == 0) goto L_0x0044
            android.hardware.display.DisplayManager r1 = r6.f40h     // Catch:{ Exception -> 0x0044 }
            android.view.Display[] r1 = r1.getDisplays()     // Catch:{ Exception -> 0x0044 }
            if (r1 == 0) goto L_0x0044
            int r1 = r1.length     // Catch:{ Exception -> 0x0044 }
            if (r1 <= 0) goto L_0x0044
            android.hardware.display.DisplayManager r1 = r6.f40h     // Catch:{ Exception -> 0x0044 }
            android.view.Display r1 = r1.getDisplay(r0)     // Catch:{ Exception -> 0x0044 }
            int r1 = r1.getRotation()     // Catch:{ Exception -> 0x0044 }
            int r2 = r6.f41i     // Catch:{ Exception -> 0x0044 }
            if (r1 == r2) goto L_0x0044
            r2 = 2
            r3 = 1
            if (r1 == 0) goto L_0x0027
            if (r1 != r2) goto L_0x0025
            goto L_0x0027
        L_0x0025:
            r4 = 0
            goto L_0x0028
        L_0x0027:
            r4 = 1
        L_0x0028:
            int r5 = r6.f41i     // Catch:{ Exception -> 0x0044 }
            if (r5 == 0) goto L_0x0033
            int r5 = r6.f41i     // Catch:{ Exception -> 0x0044 }
            if (r5 != r2) goto L_0x0031
            goto L_0x0033
        L_0x0031:
            r2 = 0
            goto L_0x0034
        L_0x0033:
            r2 = 1
        L_0x0034:
            r6.f41i = r1     // Catch:{ Exception -> 0x0044 }
            if (r4 == r2) goto L_0x0044
            int r1 = r6.f35c     // Catch:{ Exception -> 0x0044 }
            int r2 = r6.f36d     // Catch:{ Exception -> 0x0044 }
            r6.f35c = r2     // Catch:{ Exception -> 0x0044 }
            r6.f36d = r1     // Catch:{ Exception -> 0x0044 }
            r6.m49j()     // Catch:{ Exception -> 0x0044 }
            return r3
        L_0x0044:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p003a.C0012a.m45f():boolean");
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.d.a(java.lang.Exception):null, types can be incorrect */
    /* JADX WARNING: Can't wrap try/catch for region: R(19:15|16|(2:18|19)|20|21|22|(2:24|25)|26|27|(2:29|30)|31|32|(2:34|35)|36|37|(2:39|40)|41|42|(5:44|(4:46|(1:48)|67|49)|66|50|51)(5:52|53|54|59|60)) */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x03d4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x03d5, code lost:
        p002eu.chainfire.librootjava.C0078d.m287a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x03df, code lost:
        throw new java.lang.RuntimeException("CFSurface: unexpected exception during SurfaceControl creation");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0128 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0199 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0215 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x027e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x02d3 */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x012e A[SYNTHETIC, Splitter:B:24:0x012e] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x019d A[SYNTHETIC, Splitter:B:29:0x019d] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0219 A[SYNTHETIC, Splitter:B:34:0x0219] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0282 A[SYNTHETIC, Splitter:B:39:0x0282] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x02d7 A[Catch:{ Exception -> 0x03d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0322 A[Catch:{ Exception -> 0x03d4 }] */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m46g() {
        /*
            r17 = this;
            r1 = r17
            java.lang.String r0 = "android.view.SurfaceSession"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x03d4 }
            r2 = 0
            java.lang.Class[] r3 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Constructor r3 = r0.getConstructor(r3)     // Catch:{ Exception -> 0x03d4 }
            r4 = 1
            r3.setAccessible(r4)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r3 = r3.newInstance(r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f42j = r3     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "android.view.SurfaceControl"
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ Exception -> 0x03d4 }
            r1.f43k = r3     // Catch:{ Exception -> 0x03d4 }
            r3 = 0
            java.lang.Class<?> r5 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.String r6 = "getBuiltInDisplay"
            java.lang.Class[] r7 = new java.lang.Class[r4]     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.Class r8 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0041 }
            r7[r2] = r8     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.reflect.Method r5 = r5.getDeclaredMethod(r6, r7)     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.Object[] r6 = new java.lang.Object[r4]     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0041 }
            r6[r2] = r7     // Catch:{ NoSuchMethodException -> 0x0041 }
            java.lang.Object r5 = r5.invoke(r3, r6)     // Catch:{ NoSuchMethodException -> 0x0041 }
            android.os.IBinder r5 = (android.os.IBinder) r5     // Catch:{ NoSuchMethodException -> 0x0041 }
            goto L_0x0042
        L_0x0041:
            r5 = r3
        L_0x0042:
            if (r5 != 0) goto L_0x0076
            java.lang.Class<?> r5 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r6 = "getPhysicalDisplayIds"
            java.lang.Class[] r7 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r5 = r5.getDeclaredMethod(r6, r7)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r6 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r5 = r5.invoke(r3, r6)     // Catch:{ Exception -> 0x03d4 }
            long[] r5 = (long[]) r5     // Catch:{ Exception -> 0x03d4 }
            long[] r5 = (long[]) r5     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r6 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r7 = "getPhysicalDisplayToken"
            java.lang.Class[] r8 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class r9 = java.lang.Long.TYPE     // Catch:{ Exception -> 0x03d4 }
            r8[r2] = r9     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r6 = r6.getDeclaredMethod(r7, r8)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r7 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x03d4 }
            r8 = r5[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ Exception -> 0x03d4 }
            r7[r2] = r5     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r5 = r6.invoke(r3, r7)     // Catch:{ Exception -> 0x03d4 }
            android.os.IBinder r5 = (android.os.IBinder) r5     // Catch:{ Exception -> 0x03d4 }
        L_0x0076:
            java.lang.Class<?> r6 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r7 = "getDisplayConfigs"
            java.lang.Class[] r8 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<android.os.IBinder> r9 = android.os.IBinder.class
            r8[r2] = r9     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r6 = r6.getDeclaredMethod(r7, r8)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r7 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x03d4 }
            r7[r2] = r5     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r5 = r6.invoke(r3, r7)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r5 = (java.lang.Object[]) r5     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r5 = (java.lang.Object[]) r5     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r6 = "android.view.SurfaceControl$PhysicalDisplayInfo"
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r7 = "width"
            java.lang.reflect.Field r7 = r6.getDeclaredField(r7)     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r8 = "height"
            java.lang.reflect.Field r6 = r6.getDeclaredField(r8)     // Catch:{ Exception -> 0x03d4 }
            if (r5 == 0) goto L_0x03cc
            int r8 = r5.length     // Catch:{ Exception -> 0x03d4 }
            if (r8 == 0) goto L_0x03cc
            r8 = r5[r2]     // Catch:{ Exception -> 0x03d4 }
            int r7 = r7.getInt(r8)     // Catch:{ Exception -> 0x03d4 }
            r1.f35c = r7     // Catch:{ Exception -> 0x03d4 }
            r5 = r5[r2]     // Catch:{ Exception -> 0x03d4 }
            int r5 = r6.getInt(r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f36d = r5     // Catch:{ Exception -> 0x03d4 }
            r17.m45f()     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r5 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            r6 = 7
            r7 = 8
            r8 = 5
            r9 = 6
            r10 = 3
            r11 = 2
            r12 = 4
            if (r5 != 0) goto L_0x0128
            java.lang.Class<?> r5 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class[] r13 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r2] = r0     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class<java.lang.String> r14 = java.lang.String.class
            r13[r4] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r11] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r10] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r12] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r8] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class<?> r14 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13[r9] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Class<android.util.SparseIntArray> r14 = android.util.SparseIntArray.class
            r13[r6] = r14     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.reflect.Constructor r5 = r5.getDeclaredConstructor(r13)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r5.setAccessible(r4)     // Catch:{ NoSuchMethodException -> 0x0128 }
            android.util.SparseIntArray r13 = new android.util.SparseIntArray     // Catch:{ NoSuchMethodException -> 0x0128 }
            r13.<init>(r2)     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Object[] r14 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Object r15 = r1.f42j     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r2] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.String r15 = "CFSurface"
            r14[r4] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            int r15 = r1.f35c     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r11] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            int r15 = r1.f36d     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r10] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            int r15 = r17.mo68e()     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r12] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r8] = r15     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r9] = r3     // Catch:{ NoSuchMethodException -> 0x0128 }
            r14[r6] = r13     // Catch:{ NoSuchMethodException -> 0x0128 }
            java.lang.Object r5 = r5.newInstance(r14)     // Catch:{ NoSuchMethodException -> 0x0128 }
            r1.f44l = r5     // Catch:{ NoSuchMethodException -> 0x0128 }
        L_0x0128:
            java.lang.Object r5 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            r13 = 9
            if (r5 != 0) goto L_0x0199
            java.lang.Class<?> r5 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class[] r14 = new java.lang.Class[r13]     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r2] = r0     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class<java.lang.String> r15 = java.lang.String.class
            r14[r4] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r11] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r10] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r12] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r8] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class<?> r15 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r9] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r6] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Class r15 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r7] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.reflect.Constructor r5 = r5.getDeclaredConstructor(r14)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r5.setAccessible(r4)     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Object[] r14 = new java.lang.Object[r13]     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Object r15 = r1.f42j     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r2] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.String r15 = "CFSurface"
            r14[r4] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            int r15 = r1.f35c     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r11] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            int r15 = r1.f36d     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r10] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            int r15 = r17.mo68e()     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r12] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r8] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r9] = r3     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r6] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r14[r7] = r15     // Catch:{ NoSuchMethodException -> 0x0199 }
            java.lang.Object r5 = r5.newInstance(r14)     // Catch:{ NoSuchMethodException -> 0x0199 }
            r1.f44l = r5     // Catch:{ NoSuchMethodException -> 0x0199 }
        L_0x0199:
            java.lang.Object r5 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            if (r5 != 0) goto L_0x0215
            java.lang.Class<?> r5 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0215 }
            r14 = 10
            java.lang.Class[] r15 = new java.lang.Class[r14]     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r2] = r0     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class<java.lang.String> r16 = java.lang.String.class
            r15[r4] = r16     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r11] = r16     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r10] = r16     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r12] = r16     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r8] = r16     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class<?> r3 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r9] = r3     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r3 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r6] = r3     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r3 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r7] = r3     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Class r3 = java.lang.Boolean.TYPE     // Catch:{ NoSuchMethodException -> 0x0215 }
            r15[r13] = r3     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.reflect.Constructor r3 = r5.getDeclaredConstructor(r15)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r3.setAccessible(r4)     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Object[] r5 = new java.lang.Object[r14]     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Object r14 = r1.f42j     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r2] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.String r14 = "CFSurface"
            r5[r4] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            int r14 = r1.f35c     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r11] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            int r14 = r1.f36d     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r10] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            int r14 = r17.mo68e()     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r12] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r8] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            r14 = 0
            r5[r9] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r6] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r7] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Boolean r14 = java.lang.Boolean.valueOf(r4)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r5[r13] = r14     // Catch:{ NoSuchMethodException -> 0x0215 }
            java.lang.Object r3 = r3.newInstance(r5)     // Catch:{ NoSuchMethodException -> 0x0215 }
            r1.f44l = r3     // Catch:{ NoSuchMethodException -> 0x0215 }
        L_0x0215:
            java.lang.Object r3 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            if (r3 != 0) goto L_0x027e
            java.lang.Class<?> r3 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class[] r5 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r2] = r0     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r5[r4] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r11] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r10] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r12] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r8] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r9] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r6] = r13     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.reflect.Constructor r3 = r3.getDeclaredConstructor(r5)     // Catch:{ NoSuchMethodException -> 0x027e }
            r3.setAccessible(r4)     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Object[] r5 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Object r7 = r1.f42j     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r2] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.String r7 = "CFSurface"
            r5[r4] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            int r7 = r1.f35c     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r11] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            int r7 = r1.f36d     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r10] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            int r7 = r17.mo68e()     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r12] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r8] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r9] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r2)     // Catch:{ NoSuchMethodException -> 0x027e }
            r5[r6] = r7     // Catch:{ NoSuchMethodException -> 0x027e }
            java.lang.Object r3 = r3.newInstance(r5)     // Catch:{ NoSuchMethodException -> 0x027e }
            r1.f44l = r3     // Catch:{ NoSuchMethodException -> 0x027e }
        L_0x027e:
            java.lang.Object r3 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            if (r3 != 0) goto L_0x02d3
            java.lang.Class<?> r3 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class[] r5 = new java.lang.Class[r9]     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r5[r2] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class<java.lang.String> r0 = java.lang.String.class
            r5[r4] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r5[r11] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r5[r10] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r5[r12] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Class r0 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r5[r8] = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.reflect.Constructor r0 = r3.getDeclaredConstructor(r5)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r0.setAccessible(r4)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Object[] r3 = new java.lang.Object[r9]     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Object r5 = r1.f42j     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r3[r2] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.String r5 = "CFSurface"
            r3[r4] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            int r5 = r1.f35c     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r3[r11] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            int r5 = r1.f36d     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r3[r10] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            int r5 = r17.mo68e()     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r3[r12] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r12)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r3[r8] = r5     // Catch:{ NoSuchMethodException -> 0x02d3 }
            java.lang.Object r0 = r0.newInstance(r3)     // Catch:{ NoSuchMethodException -> 0x02d3 }
            r1.f44l = r0     // Catch:{ NoSuchMethodException -> 0x02d3 }
        L_0x02d3:
            java.lang.Object r0 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            if (r0 != 0) goto L_0x0322
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Constructor[] r0 = r0.getDeclaredConstructors()     // Catch:{ Exception -> 0x03d4 }
            int r3 = r0.length     // Catch:{ Exception -> 0x03d4 }
            r5 = 0
            r6 = 0
        L_0x02e0:
            if (r5 >= r3) goto L_0x031a
            r7 = r0[r5]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class[] r7 = r7.getParameterTypes()     // Catch:{ Exception -> 0x03d4 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03d4 }
            r8.<init>()     // Catch:{ Exception -> 0x03d4 }
            int r9 = r7.length     // Catch:{ Exception -> 0x03d4 }
            r10 = 0
        L_0x02ef:
            if (r10 >= r9) goto L_0x0302
            r12 = r7[r10]     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r12 = r12.getName()     // Catch:{ Exception -> 0x03d4 }
            r8.append(r12)     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r12 = " "
            r8.append(r12)     // Catch:{ Exception -> 0x03d4 }
            int r10 = r10 + 1
            goto L_0x02ef
        L_0x0302:
            java.lang.String r7 = "constructor[%d]: %s"
            java.lang.Object[] r9 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x03d4 }
            r9[r2] = r10     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x03d4 }
            r9[r4] = r8     // Catch:{ Exception -> 0x03d4 }
            p002eu.chainfire.librootjava.C0078d.m290a(r7, r9)     // Catch:{ Exception -> 0x03d4 }
            int r6 = r6 + 1
            int r5 = r5 + 1
            goto L_0x02e0
        L_0x031a:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r2 = "CFSurface: could not create SurfaceControl"
            r0.<init>(r2)     // Catch:{ Exception -> 0x03d4 }
            throw r0     // Catch:{ Exception -> 0x03d4 }
        L_0x0322:
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "openTransaction"
            java.lang.Class[] r5 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f45m = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "closeTransaction"
            java.lang.Class[] r5 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f46n = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "setLayer"
            java.lang.Class[] r5 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x03d4 }
            r5[r2] = r6     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f47o = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "show"
            java.lang.Class[] r5 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f48p = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r3 = "hide"
            java.lang.Class[] r5 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            r1.f49q = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r0 = r1.f43k     // Catch:{ NoSuchMethodException -> 0x0377 }
            java.lang.String r3 = "setSize"
            java.lang.Class[] r5 = new java.lang.Class[r11]     // Catch:{ NoSuchMethodException -> 0x0377 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0377 }
            r5[r2] = r6     // Catch:{ NoSuchMethodException -> 0x0377 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0377 }
            r5[r4] = r6     // Catch:{ NoSuchMethodException -> 0x0377 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ NoSuchMethodException -> 0x0377 }
            r1.f50r = r0     // Catch:{ NoSuchMethodException -> 0x0377 }
            goto L_0x037e
        L_0x0377:
            java.lang.String r0 = "QP1: Could not retrieve setSize method"
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            p002eu.chainfire.librootjava.C0078d.m292b(r0, r3)     // Catch:{ Exception -> 0x03d4 }
        L_0x037e:
            java.lang.Class<android.view.Surface> r0 = android.view.Surface.class
            java.lang.Class[] r3 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Constructor r0 = r0.getDeclaredConstructor(r3)     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r0 = r0.newInstance(r3)     // Catch:{ Exception -> 0x03d4 }
            android.view.Surface r0 = (android.view.Surface) r0     // Catch:{ Exception -> 0x03d4 }
            r1.f51s = r0     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<android.view.Surface> r0 = android.view.Surface.class
            java.lang.String r3 = "copyFrom"
            java.lang.Class[] r5 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Class<?> r6 = r1.f43k     // Catch:{ Exception -> 0x03d4 }
            r5[r2] = r6     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            android.view.Surface r3 = r1.f51s     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r6 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            r5[r2] = r6     // Catch:{ Exception -> 0x03d4 }
            r0.invoke(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r1.f45m     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            r5 = 0
            r0.invoke(r5, r3)     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r1.f47o     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object r3 = r1.f44l     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x03d4 }
            r6 = 2147483647(0x7fffffff, float:NaN)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x03d4 }
            r5[r2] = r6     // Catch:{ Exception -> 0x03d4 }
            r0.invoke(r3, r5)     // Catch:{ Exception -> 0x03d4 }
            java.lang.reflect.Method r0 = r1.f46n     // Catch:{ Exception -> 0x03d4 }
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x03d4 }
            r3 = 0
            r0.invoke(r3, r2)     // Catch:{ Exception -> 0x03d4 }
            return r4
        L_0x03cc:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ Exception -> 0x03d4 }
            java.lang.String r2 = "CFSurface: could not determine screen dimensions"
            r0.<init>(r2)     // Catch:{ Exception -> 0x03d4 }
            throw r0     // Catch:{ Exception -> 0x03d4 }
        L_0x03d4:
            r0 = move-exception
            p002eu.chainfire.librootjava.C0078d.m287a(r0)
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r2 = "CFSurface: unexpected exception during SurfaceControl creation"
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p003a.C0012a.m46g():boolean");
    }

    public static String getLaunchString(Context context, Class<?> cls, String str, String str2) {
        return C0083h.m313a(context, cls, str, new String[]{context.getPackageCodePath()}, str2);
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.d.a(java.lang.Exception):null, types can be incorrect */
    /* renamed from: h */
    private final boolean m47h() {
        try {
            this.f51s.release();
            this.f43k.getDeclaredMethod("release", new Class[0]).invoke(this.f44l, new Object[0]);
            return true;
        } catch (Exception e) {
            C0078d.m287a(e);
            return false;
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.d.a(java.lang.Exception):null, types can be incorrect */
    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m48i() {
        Method method;
        Object obj;
        Object[] objArr;
        if (this.f33a != this.f34b) {
            try {
                this.f45m.invoke(null, new Object[0]);
                if (this.f33a) {
                    method = this.f48p;
                    obj = this.f44l;
                    objArr = new Object[0];
                } else {
                    method = this.f49q;
                    obj = this.f44l;
                    objArr = new Object[0];
                }
                method.invoke(obj, objArr);
                this.f46n.invoke(null, new Object[0]);
            } catch (Exception e) {
                C0078d.m287a(e);
            }
            this.f34b = this.f33a;
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.d.a(java.lang.Exception):null, types can be incorrect */
    /* renamed from: j */
    private final void m49j() {
        if (this.f51s != null) {
            try {
                if (this.f50r == null) {
                    C0078d.m292b("QP1: setSize == null", new Object[0]);
                    return;
                }
                this.f45m.invoke(null, new Object[0]);
                this.f50r.invoke(this.f44l, new Object[]{Integer.valueOf(this.f35c), Integer.valueOf(this.f36d)});
                this.f46n.invoke(null, new Object[0]);
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public final void m50k() {
        long uptimeMillis = SystemClock.uptimeMillis();
        long j = uptimeMillis - this.f37e;
        if (j < 17) {
            try {
                Thread.sleep(17 - j);
            } catch (Exception unused) {
            }
        }
        this.f37e = uptimeMillis;
    }

    /* renamed from: l */
    private final Thread m51l() {
        return new Thread() {
            public void run() {
                int[] iArr = new int[2];
                EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
                if (EGL14.eglInitialize(eglGetDisplay, iArr, 0, iArr, 1)) {
                    EGLConfig[] eGLConfigArr = new EGLConfig[1];
                    if (EGL14.eglChooseConfig(eglGetDisplay, new int[]{12324, 8, 12323, 8, 12322, 8, 12321, 8, 12325, 0, 12352, 4, 12344, 12344}, 0, eGLConfigArr, 0, 1, new int[1], 0)) {
                        EGLSurface eglCreateWindowSurface = EGL14.eglCreateWindowSurface(eglGetDisplay, eGLConfigArr[0], C0012a.this.f51s, new int[]{12344, 12344}, 0);
                        if (eglCreateWindowSurface != null) {
                            EGLContext eglCreateContext = EGL14.eglCreateContext(eglGetDisplay, eGLConfigArr[0], EGL14.EGL_NO_CONTEXT, new int[]{12440, 2, 12344, 12344}, 0);
                            if (eglCreateContext == null) {
                                throw new RuntimeException("CFSurface: eglCreateContext failure");
                            } else if (EGL14.eglMakeCurrent(eglGetDisplay, eglCreateWindowSurface, eglCreateWindowSurface, eglCreateContext)) {
                                C0012a.this.mo62a(C0012a.this.f35c, C0012a.this.f36d);
                                C0012a.this.mo66c();
                                while (!isInterrupted()) {
                                    ((C0016b) C0012a.this).onGLRenderFrame();
                                    EGL14.eglSwapBuffers(eglGetDisplay, eglCreateWindowSurface);
                                    C0012a.this.m48i();
                                    if (C0012a.this.m45f()) {
                                        GLES20.glViewport(0, 0, C0012a.this.f35c, C0012a.this.f36d);
                                        C0012a.this.mo65b(C0012a.this.f35c, C0012a.this.f36d);
                                    }
                                    C0012a.this.m50k();
                                }
                                C0012a.this.mo67d();
                                EGL14.eglMakeCurrent(eglGetDisplay, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_SURFACE, EGL14.EGL_NO_CONTEXT);
                                EGL14.eglDestroyContext(eglGetDisplay, eglCreateContext);
                                EGL14.eglDestroySurface(eglGetDisplay, eglCreateWindowSurface);
                                EGL14.eglTerminate(eglGetDisplay);
                            } else {
                                throw new RuntimeException("CFSurface: eglCreateContext failure");
                            }
                        } else {
                            throw new RuntimeException("CFSurface: eglCreateWindowSurface failure");
                        }
                    } else {
                        throw new RuntimeException("CFSurface: eglChooseConfig failure");
                    }
                } else {
                    throw new RuntimeException("CFSurface: eglInitialize failure");
                }
            }
        };
    }

    /* renamed from: m */
    private final Thread m52m() {
        return new Thread() {
            public void run() {
                Canvas canvas;
                C0012a.this.mo62a(C0012a.this.f35c, C0012a.this.f36d);
                C0012a.this.mo66c();
                while (!isInterrupted()) {
                    if (C0012a.this instanceof C0018d) {
                        ((C0018d) C0012a.this).mo75a(C0012a.this.f51s);
                    } else {
                        if (C0012a.this instanceof C0015a) {
                            canvas = C0012a.this.f51s.lockCanvas(null);
                            try {
                                ((C0015a) C0012a.this).mo72a(canvas);
                            } catch (Throwable th) {
                                C0012a.this.f51s.unlockCanvasAndPost(canvas);
                                throw th;
                            }
                        } else if (C0012a.this instanceof C0017c) {
                            canvas = VERSION.SDK_INT >= 23 ? C0012a.this.f51s.lockHardwareCanvas() : C0012a.this.f51s.lockCanvas(null);
                            try {
                                ((C0017c) C0012a.this).mo74a(canvas);
                            } catch (Throwable th2) {
                                C0012a.this.f51s.unlockCanvasAndPost(canvas);
                                throw th2;
                            }
                        }
                        C0012a.this.f51s.unlockCanvasAndPost(canvas);
                    }
                    C0012a.this.m48i();
                    if (C0012a.this.m45f()) {
                        C0012a.this.mo65b(C0012a.this.f35c, C0012a.this.f36d);
                    }
                    C0012a.this.m50k();
                }
                C0012a.this.mo67d();
            }
        };
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo61a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo62a(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo63a(String[] strArr);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo64b();

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo65b(int i, int i2);

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract void mo66c();

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract void mo67d();

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public int mo68e() {
        if (VERSION.SDK_INT >= 29 || (VERSION.SDK_INT == 28 && VERSION.PREVIEW_SDK_INT != 0)) {
            return 1;
        }
        return VERSION.SDK_INT >= 24 ? 3 : 4;
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.d.a(java.lang.Exception):null, types can be incorrect */
    public final void run(String[] strArr) {
        if (!(this instanceof C0016b) || (this instanceof C0015a) || (this instanceof C0017c) || (this instanceof C0018d)) {
            throw new RuntimeException("CFSurface: no render callback implemented");
        } else if (strArr != null) {
            try {
                if (strArr.length >= 1) {
                    this.f38f = strArr[0];
                    C0083h.m317a();
                    String[] strArr2 = new String[(strArr.length - 1)];
                    int i = 0;
                    while (i < strArr.length - 1) {
                        int i2 = i + 1;
                        strArr2[i] = strArr[i2];
                        i = i2;
                    }
                    this.f39g = C0083h.m318b();
                    this.f40h = (DisplayManager) this.f39g.getSystemService("display");
                    mo63a(strArr2);
                    m46g();
                    Thread l = this instanceof C0016b ? m51l() : m52m();
                    l.start();
                    mo61a();
                    l.interrupt();
                    l.join();
                    m47h();
                    mo64b();
                    System.exit(0);
                }
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        }
    }
}
