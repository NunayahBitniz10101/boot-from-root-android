package p002eu.chainfire.p003a.p004a;

import android.opengl.GLES20;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import p002eu.chainfire.librootjava.C0078d;

/* renamed from: eu.chainfire.a.a.f */
public class C0028f {
    /* renamed from: a */
    public static int m95a(int i, int i2, String[] strArr) {
        int glCreateProgram = GLES20.glCreateProgram();
        m99a((String) "glCreateProgram");
        GLES20.glAttachShader(glCreateProgram, i);
        GLES20.glAttachShader(glCreateProgram, i2);
        if (strArr != null) {
            int length = strArr.length;
            for (int i3 = 0; i3 < length; i3++) {
                GLES20.glBindAttribLocation(glCreateProgram, i3, strArr[i3]);
            }
        }
        GLES20.glLinkProgram(glCreateProgram);
        m99a((String) "glLinkProgram");
        GLES20.glDeleteShader(i);
        GLES20.glDeleteShader(i2);
        return glCreateProgram;
    }

    /* renamed from: a */
    public static int m96a(int i, String str) {
        int glCreateShader = GLES20.glCreateShader(i);
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        m99a((String) "glCompileShader");
        return glCreateShader;
    }

    /* renamed from: a */
    public static FloatBuffer m97a(int i) {
        FloatBuffer asFloatBuffer = ByteBuffer.allocateDirect(i * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        asFloatBuffer.position(0);
        return asFloatBuffer;
    }

    /* renamed from: a */
    public static FloatBuffer m98a(float[] fArr) {
        FloatBuffer a = m97a(fArr.length);
        a.put(fArr);
        a.position(0);
        return a;
    }

    /* renamed from: a */
    public static void m99a(String str) {
        while (true) {
            int glGetError = GLES20.glGetError();
            if (glGetError != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(": glError ");
                sb.append(glGetError);
                C0078d.m290a(sb.toString(), new Object[0]);
            } else {
                return;
            }
        }
    }
}
