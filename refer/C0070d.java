package p002eu.chainfire.p005b;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/* renamed from: eu.chainfire.b.d */
public class C0070d extends Thread {

    /* renamed from: a */
    private static int f233a;

    /* renamed from: b */
    private final String f234b;

    /* renamed from: c */
    private final InputStream f235c;

    /* renamed from: d */
    private final BufferedReader f236d;

    /* renamed from: e */
    private final List<String> f237e;

    /* renamed from: f */
    private final C0071a f238f;

    /* renamed from: g */
    private final C0072b f239g;

    /* renamed from: h */
    private volatile boolean f240h = true;

    /* renamed from: eu.chainfire.b.d$a */
    public interface C0071a {
        /* renamed from: a */
        void mo143a(String str);
    }

    /* renamed from: eu.chainfire.b.d$b */
    public interface C0072b {
        /* renamed from: a */
        void mo142a();
    }

    /* JADX WARN: Illegal instructions before constructor call commented (this can break semantics) */
    public C0070d(String str, InputStream inputStream, C0071a aVar, C0072b bVar) {
        // StringBuilder sb = new StringBuilder();
        // sb.append("Gobbler#");
        // sb.append(m258g());
        super(sb.toString());
        this.f234b = str;
        this.f235c = inputStream;
        this.f236d = new BufferedReader(new InputStreamReader(inputStream));
        this.f238f = aVar;
        this.f239g = bVar;
        this.f237e = null;
    }

    /* JADX WARN: Illegal instructions before constructor call commented (this can break semantics) */
    public C0070d(String str, InputStream inputStream, List<String> list) {
        // StringBuilder sb = new StringBuilder();
        // sb.append("Gobbler#");
        // sb.append(m258g());
        super(sb.toString());
        this.f234b = str;
        this.f235c = inputStream;
        this.f236d = new BufferedReader(new InputStreamReader(inputStream));
        this.f237e = list;
        this.f238f = null;
        this.f239g = null;
    }

    /* renamed from: g */
    private static int m258g() {
        int i;
        synchronized (C0070d.class) {
            i = f233a;
            f233a++;
        }
        return i;
    }

    /* renamed from: a */
    public void mo158a() {
        if (!this.f240h) {
            synchronized (this) {
                this.f240h = true;
                notifyAll();
            }
        }
    }

    /* renamed from: b */
    public void mo159b() {
        synchronized (this) {
            this.f240h = false;
            notifyAll();
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:1:0x0001 */
    /* JADX WARNING: Removed duplicated region for block: B:1:0x0001 A[LOOP:0: B:1:0x0001->B:13:0x0001, LOOP_START, SYNTHETIC] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo160c() {
        /*
            r2 = this;
            monitor-enter(r2)
        L_0x0001:
            boolean r0 = r2.f240h     // Catch:{ all -> 0x000d }
            if (r0 == 0) goto L_0x000b
            r0 = 32
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0001 }
            goto L_0x0001
        L_0x000b:
            monitor-exit(r2)     // Catch:{ all -> 0x000d }
            return
        L_0x000d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x000d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0070d.mo160c():void");
    }

    /* renamed from: d */
    public boolean mo161d() {
        boolean z;
        synchronized (this) {
            z = !this.f240h;
        }
        return z;
    }

    /* renamed from: e */
    public InputStream mo162e() {
        return this.f235c;
    }

    /* renamed from: f */
    public C0071a mo163f() {
        return this.f238f;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x003d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
        L_0x0000:
            r0 = 1
            r1 = 0
            java.io.BufferedReader r2 = r7.f236d     // Catch:{ IOException -> 0x0041 }
            java.lang.String r2 = r2.readLine()     // Catch:{ IOException -> 0x0041 }
            if (r2 == 0) goto L_0x004c
            java.util.Locale r3 = java.util.Locale.ENGLISH     // Catch:{ IOException -> 0x0041 }
            java.lang.String r4 = "[%s] %s"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ IOException -> 0x0041 }
            java.lang.String r6 = r7.f234b     // Catch:{ IOException -> 0x0041 }
            r5[r1] = r6     // Catch:{ IOException -> 0x0041 }
            r5[r0] = r2     // Catch:{ IOException -> 0x0041 }
            java.lang.String r3 = java.lang.String.format(r3, r4, r5)     // Catch:{ IOException -> 0x0041 }
            p002eu.chainfire.p005b.C0029a.m108c(r3)     // Catch:{ IOException -> 0x0041 }
            java.util.List<java.lang.String> r3 = r7.f237e     // Catch:{ IOException -> 0x0041 }
            if (r3 == 0) goto L_0x0027
            java.util.List<java.lang.String> r3 = r7.f237e     // Catch:{ IOException -> 0x0041 }
            r3.add(r2)     // Catch:{ IOException -> 0x0041 }
        L_0x0027:
            eu.chainfire.b.d$a r3 = r7.f238f     // Catch:{ IOException -> 0x0041 }
            if (r3 == 0) goto L_0x0030
            eu.chainfire.b.d$a r3 = r7.f238f     // Catch:{ IOException -> 0x0041 }
            r3.mo143a(r2)     // Catch:{ IOException -> 0x0041 }
        L_0x0030:
            boolean r2 = r7.f240h     // Catch:{ IOException -> 0x0041 }
            if (r2 != 0) goto L_0x0000
            monitor-enter(r7)     // Catch:{ IOException -> 0x0041 }
            r2 = 128(0x80, double:6.32E-322)
            r7.wait(r2)     // Catch:{ InterruptedException -> 0x003d }
            goto L_0x003d
        L_0x003b:
            r2 = move-exception
            goto L_0x003f
        L_0x003d:
            monitor-exit(r7)     // Catch:{ all -> 0x003b }
            goto L_0x0030
        L_0x003f:
            monitor-exit(r7)     // Catch:{ all -> 0x003b }
            throw r2     // Catch:{ IOException -> 0x0041 }
        L_0x0041:
            eu.chainfire.b.d$b r2 = r7.f239g
            if (r2 == 0) goto L_0x004c
            eu.chainfire.b.d$b r1 = r7.f239g
            r1.mo142a()
            goto L_0x004d
        L_0x004c:
            r0 = 0
        L_0x004d:
            java.io.BufferedReader r1 = r7.f236d     // Catch:{ IOException -> 0x0052 }
            r1.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0052:
            if (r0 != 0) goto L_0x005d
            eu.chainfire.b.d$b r0 = r7.f239g
            if (r0 == 0) goto L_0x005d
            eu.chainfire.b.d$b r0 = r7.f239g
            r0.mo142a()
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p005b.C0070d.run():void");
    }
}
