package p002eu.chainfire.librootjava;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: eu.chainfire.librootjava.IRootIPC */
public interface IRootIPC extends IInterface {

    /* renamed from: eu.chainfire.librootjava.IRootIPC$Stub */
    public static abstract class Stub extends Binder implements IRootIPC {
        private static final String DESCRIPTOR = "eu.chainfire.librootjava.IRootIPC";
        static final int TRANSACTION_bye = 3;
        static final int TRANSACTION_getUserIPC = 2;
        static final int TRANSACTION_hello = 1;

        /* renamed from: eu.chainfire.librootjava.IRootIPC$Stub$Proxy */
        private static class Proxy implements IRootIPC {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void bye(IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(Stub.TRANSACTION_bye, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public IBinder getUserIPC() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_getUserIPC, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readStrongBinder();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void hello(IBinder iBinder) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeStrongBinder(iBinder);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IRootIPC asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            return (queryLocalInterface == null || !(queryLocalInterface instanceof IRootIPC)) ? new Proxy(iBinder) : (IRootIPC) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i != 1598968902) {
                switch (i) {
                    case 1:
                        parcel.enforceInterface(DESCRIPTOR);
                        hello(parcel.readStrongBinder());
                        parcel2.writeNoException();
                        return true;
                    case TRANSACTION_getUserIPC /*2*/:
                        parcel.enforceInterface(DESCRIPTOR);
                        IBinder userIPC = getUserIPC();
                        parcel2.writeNoException();
                        parcel2.writeStrongBinder(userIPC);
                        return true;
                    case TRANSACTION_bye /*3*/:
                        parcel.enforceInterface(DESCRIPTOR);
                        bye(parcel.readStrongBinder());
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    void bye(IBinder iBinder);

    IBinder getUserIPC();

    void hello(IBinder iBinder);
}
