package p002eu.chainfire.librootjava;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import java.lang.reflect.Method;

/* renamed from: eu.chainfire.librootjava.f */
class C0080f {

    /* renamed from: a */
    private static final Object f252a = new Object();
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: b */
    private static Context f253b = null;

    /* renamed from: c */
    private static Object f254c = null;

    /* renamed from: d */
    private static Integer f255d = null;

    /* renamed from: e */
    private static Method f256e = null;

    @SuppressLint({"PrivateApi"})
    /* renamed from: a */
    static Context m295a() {
        synchronized (f252a) {
            try {
                if (f253b != null) {
                    Context context = f253b;
                    return context;
                }
                if (Looper.getMainLooper() == null) {
                    try {
                        Looper.prepareMainLooper();
                    } catch (Exception e) {
                        C0078d.m287a(e);
                    }
                }
                Class<?> cls = Class.forName("android.app.ActivityThread");
                f253b = (Context) cls.getMethod("getSystemContext", new Class[0]).invoke(cls.getMethod("systemMain", new Class[0]).invoke(null, new Object[0]), new Object[0]);
                Context context2 = f253b;
                return context2;
            } catch (Exception e2) {
                C0078d.m287a(e2);
                throw new RuntimeException("librootjava: unexpected exception in getSystemContext()");
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* renamed from: a */
    private static Method m296a(Class<?> cls) {
        synchronized (f252a) {
            if (f256e != null) {
                Method method = f256e;
                return method;
            }
            Method[] methods = cls.getMethods();
            int length = methods.length;
            int i = 0;
            while (i < length) {
                Method method2 = methods[i];
                if (method2.getName().equals("broadcastIntent") && method2.getParameterTypes().length == 13) {
                    f256e = method2;
                    Method method3 = f256e;
                    return method3;
                } else if (!method2.getName().equals("broadcastIntent") || method2.getParameterTypes().length != 12) {
                    i++;
                } else {
                    f256e = method2;
                    Method method4 = f256e;
                    return method4;
                }
            }
            throw new RuntimeException("librootjava: unable to retrieve broadcastIntent method");
        }
    }

    @SuppressLint({"PrivateApi"})
    /* renamed from: a */
    static void m297a(Intent intent) {
        Intent intent2 = intent;
        try {
            intent2.setFlags(m299c());
            Object b = m298b();
            Method a = m296a(b.getClass());
            if (a.getParameterTypes().length == 13) {
                a.invoke(b, new Object[]{null, intent2, null, null, Integer.valueOf(0), null, null, null, Integer.valueOf(-1), null, Boolean.valueOf(false), Boolean.valueOf(false), Integer.valueOf(0)});
            } else if (a.getParameterTypes().length == 12) {
                a.invoke(b, new Object[]{null, intent2, null, null, Integer.valueOf(0), null, null, null, Integer.valueOf(-1), Boolean.valueOf(false), Boolean.valueOf(false), Integer.valueOf(0)});
            } else {
                throw new RuntimeException("librootjava: unable to send broadcast");
            }
        } catch (Exception e) {
            C0078d.m287a(e);
        }
    }

    @SuppressLint({"PrivateApi"})
    /* renamed from: b */
    private static Object m298b() {
        synchronized (f252a) {
            if (f254c != null) {
                Object obj = f254c;
                return obj;
            }
            try {
                f254c = Class.forName("android.app.ActivityManagerNative").getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
                Object obj2 = f254c;
                return obj2;
            } catch (Exception e) {
                C0078d.m287a(e);
                throw new RuntimeException("librootjava: unable to retrieve ActivityManager");
            } catch (Exception unused) {
                f254c = Class.forName("android.app.ActivityManager").getMethod("getService", new Class[0]).invoke(null, new Object[0]);
                return f254c;
            }
        }
    }

    /* renamed from: c */
    private static int m299c() {
        synchronized (f252a) {
            if (f255d != null) {
                int intValue = f255d.intValue();
                return intValue;
            }
            try {
                f255d = Integer.valueOf(Intent.class.getDeclaredField("FLAG_RECEIVER_FROM_SHELL").getInt(null));
                int intValue2 = f255d.intValue();
                return intValue2;
            } catch (NoSuchFieldException unused) {
                f255d = Integer.valueOf(0);
                int intValue3 = f255d.intValue();
                return intValue3;
            } catch (IllegalAccessException e) {
                C0078d.m287a((Exception) e);
                f255d = Integer.valueOf(0);
                int intValue32 = f255d.intValue();
                return intValue32;
            }
        }
    }
}
