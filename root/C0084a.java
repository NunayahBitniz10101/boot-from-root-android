package p002eu.chainfire.librootjavadaemon;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import p002eu.chainfire.librootjava.C0075a;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.librootjava.C0081g;
import p002eu.chainfire.librootjava.C0083h;

/* renamed from: eu.chainfire.librootjavadaemon.a */
public class C0084a {

    /* renamed from: a */
    public static final String[] f269a = {".daemonize_"};
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static String f270b = "daemon";
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final List<C0081g> f271c = new ArrayList();

    /* renamed from: d */
    private static volatile C0086a f272d = null;

    /* renamed from: e */
    private static final Object f273e = new Object();

    /* renamed from: eu.chainfire.librootjavadaemon.a$a */
    public interface C0086a {
        /* renamed from: a */
        void mo184a();
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> m319a(android.content.Context r0, java.lang.Class<?> r1, java.lang.String r2, java.lang.String r3, java.lang.String[] r4, java.lang.String r5) {
        /*
            java.util.List r1 = p002eu.chainfire.librootjava.C0083h.m316a(r0, r1, r2, r3, r4, r5)
            java.util.List r0 = m320a(r0, r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.librootjavadaemon.C0084a.m319a(android.content.Context, java.lang.Class, java.lang.String, java.lang.String, java.lang.String[], java.lang.String):java.util.List");
    }

    /* renamed from: a */
    public static List<String> m320a(Context context, List<String> list) {
        String str;
        ArrayList arrayList = new ArrayList();
        String str2 = null;
        boolean z = false;
        for (String next : list) {
            if (next.startsWith("#app_process=")) {
                str2 = next.substring("#app_process=".length());
            } else if (str2 != null && next.contains(str2) && next.contains("CLASSPATH=")) {
                String substring = str2.substring(0, str2.lastIndexOf(47));
                String a = C0083h.m314a(context, "daemonize");
                if (substring.startsWith("/system/bin")) {
                    str = a;
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append(substring);
                    sb.append("/.daemonize_");
                    sb.append(C0075a.f245b);
                    str = sb.toString();
                    boolean startsWith = str.startsWith("/data/");
                    arrayList.add(String.format(Locale.ENGLISH, "%s cp %s %s >/dev/null 2>/dev/null", new Object[]{C0075a.f244a, a, str}));
                    Locale locale = Locale.ENGLISH;
                    Object[] objArr = new Object[3];
                    objArr[0] = C0075a.f244a;
                    objArr[1] = startsWith ? "0766" : "0700";
                    objArr[2] = str;
                    arrayList.add(String.format(locale, "%s chmod %s %s >/dev/null 2>/dev/null", objArr));
                    if (startsWith) {
                        arrayList.add(String.format(Locale.ENGLISH, "restorecon %s >/dev/null 2>/dev/null", new Object[]{str}));
                    }
                }
                int indexOf = next.indexOf(str2);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(next.substring(0, indexOf));
                sb2.append(str);
                sb2.append(" ");
                sb2.append(next.substring(indexOf));
                arrayList.add(sb2.toString());
                z = true;
            } else if (z && next.contains("box rm")) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("#");
                sb3.append(next);
                next = sb3.toString();
            }
            arrayList.add(next);
        }
        return arrayList;
    }

    /* renamed from: a */
    public static void m321a() {
        C0078d.m289a(f270b, "Exiting", new Object[0]);
        try {
            if (f272d != null) {
                f272d.mo184a();
            }
        } catch (Exception e) {
            C0078d.m287a(e);
        }
        try {
            File canonicalFile = new File("/proc/self/exe").getCanonicalFile();
            if (canonicalFile.exists() && !canonicalFile.getAbsolutePath().startsWith("/system/bin/")) {
                canonicalFile.delete();
                File file = new File(canonicalFile.getAbsolutePath().replace(".app_process32_", ".daemonize_").replace(".app_process64_", ".daemonize_"));
                if (file.exists()) {
                    file.delete();
                }
            }
        } catch (IOException unused) {
        }
        System.exit(0);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x00cd */
    @android.annotation.SuppressLint({"PrivateApi"})
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void m322a(java.lang.String r10, int r11, boolean r12, p002eu.chainfire.librootjavadaemon.C0084a.C0086a r13) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r10)
            java.lang.String r10 = "#"
            r0.append(r10)
            java.lang.String r10 = java.lang.String.valueOf(r11)
            r0.append(r10)
            java.lang.String r10 = "#daemonize"
            r0.append(r10)
            java.lang.String r10 = r0.toString()
            java.io.File r11 = new java.io.File
            java.lang.String r0 = "CLASSPATH"
            java.lang.String r0 = java.lang.System.getenv(r0)
            r11.<init>(r0)
            java.util.Locale r0 = java.util.Locale.ENGLISH
            java.lang.String r1 = "%s:%d:%d"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = r11.getAbsolutePath()
            r4 = 0
            r2[r4] = r3
            long r5 = r11.lastModified()
            java.lang.Long r3 = java.lang.Long.valueOf(r5)
            r5 = 1
            r2[r5] = r3
            long r6 = r11.length()
            java.lang.Long r11 = java.lang.Long.valueOf(r6)
            r3 = 2
            r2[r3] = r11
            java.lang.String r11 = java.lang.String.format(r0, r1, r2)
            java.lang.String r0 = "android.os.ServiceManager"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x00df }
            java.lang.String r1 = "getService"
            java.lang.Class[] r2 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x00df }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r2[r4] = r6     // Catch:{ Exception -> 0x00df }
            java.lang.reflect.Method r1 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x00df }
            java.lang.String r2 = "addService"
            java.lang.Class[] r6 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00df }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r6[r4] = r7     // Catch:{ Exception -> 0x00df }
            java.lang.Class<android.os.IBinder> r7 = android.os.IBinder.class
            r6[r5] = r7     // Catch:{ Exception -> 0x00df }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r2, r6)     // Catch:{ Exception -> 0x00df }
            java.lang.Object[] r2 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00df }
            r2[r4] = r10     // Catch:{ Exception -> 0x00df }
            r6 = 0
            java.lang.Object r2 = r1.invoke(r6, r2)     // Catch:{ Exception -> 0x00df }
            android.os.IBinder r2 = (android.os.IBinder) r2     // Catch:{ Exception -> 0x00df }
            if (r2 == 0) goto L_0x00a9
            eu.chainfire.librootjavadaemon.IRootDaemonIPC r2 = p002eu.chainfire.librootjavadaemon.IRootDaemonIPC.Stub.asInterface(r2)     // Catch:{ Exception -> 0x00df }
            java.lang.String r7 = r2.getVersion()     // Catch:{ Exception -> 0x00df }
            boolean r7 = r7.equals(r11)     // Catch:{ Exception -> 0x00df }
            if (r7 != 0) goto L_0x009a
            java.lang.String r7 = f270b     // Catch:{ Exception -> 0x00df }
            java.lang.String r8 = "Terminating outdated daemon"
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x00df }
            p002eu.chainfire.librootjava.C0078d.m289a(r7, r8, r9)     // Catch:{ Exception -> 0x00df }
            r2.terminate()     // Catch:{ RemoteException -> 0x00a9 }
            goto L_0x00a9
        L_0x009a:
            java.lang.String r7 = f270b     // Catch:{ Exception -> 0x00df }
            java.lang.String r8 = "Service already running, requesting re-broadcast and aborting"
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x00df }
            p002eu.chainfire.librootjava.C0078d.m289a(r7, r8, r9)     // Catch:{ Exception -> 0x00df }
            r2.broadcast()     // Catch:{ Exception -> 0x00df }
            m321a()     // Catch:{ Exception -> 0x00df }
        L_0x00a9:
            java.lang.String r2 = f270b     // Catch:{ Exception -> 0x00df }
            java.lang.String r7 = "Installing service"
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x00df }
            p002eu.chainfire.librootjava.C0078d.m289a(r2, r7, r8)     // Catch:{ Exception -> 0x00df }
            f272d = r13     // Catch:{ Exception -> 0x00df }
            if (r12 != 0) goto L_0x00d0
            java.lang.Object[] r12 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00df }
            java.lang.String r13 = "activity"
            r12[r4] = r13     // Catch:{ Exception -> 0x00df }
            java.lang.Object r12 = r1.invoke(r6, r12)     // Catch:{ Exception -> 0x00df }
            android.os.IBinder r12 = (android.os.IBinder) r12     // Catch:{ Exception -> 0x00df }
            if (r12 == 0) goto L_0x00d0
            eu.chainfire.librootjavadaemon.a$1 r13 = new eu.chainfire.librootjavadaemon.a$1     // Catch:{ RemoteException -> 0x00cd }
            r13.<init>()     // Catch:{ RemoteException -> 0x00cd }
            r12.linkToDeath(r13, r4)     // Catch:{ RemoteException -> 0x00cd }
            goto L_0x00d0
        L_0x00cd:
            m321a()     // Catch:{ Exception -> 0x00df }
        L_0x00d0:
            java.lang.Object[] r12 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00df }
            r12[r4] = r10     // Catch:{ Exception -> 0x00df }
            eu.chainfire.librootjavadaemon.RootDaemon$2 r10 = new eu.chainfire.librootjavadaemon.RootDaemon$2     // Catch:{ Exception -> 0x00df }
            r10.<init>(r11)     // Catch:{ Exception -> 0x00df }
            r12[r5] = r10     // Catch:{ Exception -> 0x00df }
            r0.invoke(r6, r12)     // Catch:{ Exception -> 0x00df }
            return
        L_0x00df:
            r10 = move-exception
            p002eu.chainfire.librootjava.C0078d.m287a(r10)
            java.lang.RuntimeException r10 = new java.lang.RuntimeException
            java.lang.String r11 = "librootjavadaemon: could not get/add service"
            r10.<init>(r11)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.librootjavadaemon.C0084a.m322a(java.lang.String, int, boolean, eu.chainfire.librootjavadaemon.a$a):void");
    }
}
