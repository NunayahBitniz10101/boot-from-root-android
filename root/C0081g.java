package p002eu.chainfire.librootjava;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import java.util.List;

/* renamed from: eu.chainfire.librootjava.g */
public class C0081g {

    /* renamed from: a */
    private final String f257a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final IBinder f258b;

    /* renamed from: c */
    private final int f259c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final Object f260d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final Object f261e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final List<C0082a> f262f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public volatile boolean f263g;

    /* renamed from: h */
    private final IBinder f264h;

    /* renamed from: eu.chainfire.librootjava.g$a */
    private class C0082a {

        /* renamed from: b */
        private final IBinder f266b;

        /* renamed from: c */
        private final DeathRecipient f267c;

        public C0082a(IBinder iBinder, DeathRecipient deathRecipient) {
            this.f266b = iBinder;
            this.f267c = deathRecipient;
        }

        /* renamed from: a */
        public IBinder mo174a() {
            return this.f266b;
        }

        /* renamed from: b */
        public DeathRecipient mo175b() {
            return this.f267c;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C0082a m300a(DeathRecipient deathRecipient) {
        synchronized (this.f262f) {
            m307b();
            for (C0082a next : this.f262f) {
                if (next.mo175b() == deathRecipient) {
                    return next;
                }
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C0082a m301a(IBinder iBinder) {
        synchronized (this.f262f) {
            m307b();
            for (C0082a next : this.f262f) {
                if (next.mo174a() == iBinder) {
                    return next;
                }
            }
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0062, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m307b() {
        /*
            r4 = this;
            java.util.List<eu.chainfire.librootjava.g$a> r0 = r4.f262f
            monitor-enter(r0)
            java.util.List<eu.chainfire.librootjava.g$a> r1 = r4.f262f     // Catch:{ all -> 0x0063 }
            int r1 = r1.size()     // Catch:{ all -> 0x0063 }
            if (r1 != 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            return
        L_0x000d:
            java.util.List<eu.chainfire.librootjava.g$a> r1 = r4.f262f     // Catch:{ all -> 0x0063 }
            int r1 = r1.size()     // Catch:{ all -> 0x0063 }
            r2 = 1
            int r1 = r1 - r2
        L_0x0015:
            if (r1 < 0) goto L_0x0031
            java.util.List<eu.chainfire.librootjava.g$a> r3 = r4.f262f     // Catch:{ all -> 0x0063 }
            java.lang.Object r3 = r3.get(r1)     // Catch:{ all -> 0x0063 }
            eu.chainfire.librootjava.g$a r3 = (p002eu.chainfire.librootjava.C0081g.C0082a) r3     // Catch:{ all -> 0x0063 }
            android.os.IBinder r3 = r3.mo174a()     // Catch:{ all -> 0x0063 }
            boolean r3 = r3.isBinderAlive()     // Catch:{ all -> 0x0063 }
            if (r3 != 0) goto L_0x002e
            java.util.List<eu.chainfire.librootjava.g$a> r3 = r4.f262f     // Catch:{ all -> 0x0063 }
            r3.remove(r1)     // Catch:{ all -> 0x0063 }
        L_0x002e:
            int r1 = r1 + -1
            goto L_0x0015
        L_0x0031:
            boolean r1 = r4.f263g     // Catch:{ all -> 0x0063 }
            if (r1 != 0) goto L_0x004c
            java.util.List<eu.chainfire.librootjava.g$a> r1 = r4.f262f     // Catch:{ all -> 0x0063 }
            int r1 = r1.size()     // Catch:{ all -> 0x0063 }
            if (r1 <= 0) goto L_0x004c
            r4.f263g = r2     // Catch:{ all -> 0x0063 }
            java.lang.Object r1 = r4.f260d     // Catch:{ all -> 0x0063 }
            monitor-enter(r1)     // Catch:{ all -> 0x0063 }
            java.lang.Object r2 = r4.f260d     // Catch:{ all -> 0x0049 }
            r2.notifyAll()     // Catch:{ all -> 0x0049 }
            monitor-exit(r1)     // Catch:{ all -> 0x0049 }
            goto L_0x004c
        L_0x0049:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0049 }
            throw r2     // Catch:{ all -> 0x0063 }
        L_0x004c:
            java.util.List<eu.chainfire.librootjava.g$a> r1 = r4.f262f     // Catch:{ all -> 0x0063 }
            int r1 = r1.size()     // Catch:{ all -> 0x0063 }
            if (r1 != 0) goto L_0x0061
            java.lang.Object r1 = r4.f261e     // Catch:{ all -> 0x0063 }
            monitor-enter(r1)     // Catch:{ all -> 0x0063 }
            java.lang.Object r2 = r4.f261e     // Catch:{ all -> 0x005e }
            r2.notifyAll()     // Catch:{ all -> 0x005e }
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            goto L_0x0061
        L_0x005e:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005e }
            throw r2     // Catch:{ all -> 0x0063 }
        L_0x0061:
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            return
        L_0x0063:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0063 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.librootjava.C0081g.m307b():void");
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.f.a(android.content.Intent):null, types can be incorrect */
    /* renamed from: a */
    public void mo173a() {
        Intent intent = new Intent();
        intent.setPackage(this.f257a);
        intent.setAction("eu.chainfire.librootjava.RootIPCReceiver.BROADCAST");
        intent.setFlags(268435456);
        Bundle bundle = new Bundle();
        bundle.putBinder("binder", this.f264h);
        bundle.putInt("code", this.f259c);
        intent.putExtra("eu.chainfire.librootjava.RootIPCReceiver.BROADCAST.EXTRA", bundle);
        C0080f.m297a(intent);
    }
}
