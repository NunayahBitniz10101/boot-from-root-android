package p002eu.chainfire.liveboot;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import java.util.Locale;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.liveboot.C0144b.C0147b;
import p002eu.chainfire.liveboot.p006a.C0131b;

/* renamed from: eu.chainfire.liveboot.SettingsFragment */
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

    /* renamed from: a */
    private String f279a = "";

    /* renamed from: b */
    private SharedPreferences f280b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C0149d f281c = null;

    /* renamed from: d */
    private MultiSelectListPreference f282d = null;

    /* renamed from: e */
    private MultiSelectListPreference f283e = null;

    /* renamed from: f */
    private ListPreference f284f = null;

    /* renamed from: g */
    private ListPreference f285g = null;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public C0121a f286h = null;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public volatile boolean f287i = false;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public volatile boolean f288j = false;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public C0147b f289k = C0147b.SU_D;

    /* renamed from: eu.chainfire.liveboot.SettingsFragment$a */
    private class C0111a extends AsyncTask<Void, Void, Integer> {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final Activity f323b;

        /* renamed from: c */
        private ProgressDialog f324c = null;

        public C0111a(Activity activity) {
            this.f323b = activity;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m349a() {
            SettingsFragment.this.setPreferenceScreen(SettingsFragment.this.m331a());
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m351b() {
            this.f323b.finish();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x01c1, code lost:
            if (p002eu.chainfire.liveboot.SettingsFragment.m343c(r13.f322a).f479o.mo266a() != false) goto L_0x01d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            java.lang.Thread.sleep(32);
         */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0209  */
        /* JADX WARNING: Removed duplicated region for block: B:137:0x020c  */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0131  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Integer doInBackground(java.lang.Void... r14) {
            /*
                r13 = this;
                android.app.Activity r14 = r13.f323b
                p002eu.chainfire.liveboot.C0144b.m442c(r14)
                r14 = 1
                boolean[] r0 = new boolean[r14]
                r1 = 0
                r0[r1] = r1
                eu.chainfire.b.c$a r2 = new eu.chainfire.b.c$a
                r2.<init>()
                eu.chainfire.b.c$a r2 = r2.mo114b()
                java.lang.String r3 = "id"
                eu.chainfire.liveboot.SettingsFragment$a$1 r4 = new eu.chainfire.liveboot.SettingsFragment$a$1
                r4.<init>(r0)
                eu.chainfire.b.c$a r2 = r2.mo111a(r3, r1, r4)
                eu.chainfire.b.c$c r2 = r2.mo118c()
            L_0x0023:
                boolean r3 = r2.mo133g()
                if (r3 == 0) goto L_0x003c
                monitor-enter(r0)
                boolean r3 = r0[r1]     // Catch:{ all -> 0x0039 }
                if (r3 == 0) goto L_0x0030
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                goto L_0x003c
            L_0x0030:
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                r3 = 32
                java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x0037 }
                goto L_0x0023
            L_0x0037:
                goto L_0x0023
            L_0x0039:
                r14 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0039 }
                throw r14
            L_0x003c:
                boolean r2 = r0[r1]
                if (r2 != 0) goto L_0x0045
                java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
                return r14
            L_0x0045:
                p002eu.chainfire.p005b.C0032c.C0063q.m243a()
                java.lang.String r2 = p002eu.chainfire.p005b.C0032c.C0063q.m240a(r1)
                java.lang.String r3 = p002eu.chainfire.p005b.C0032c.C0063q.m240a(r14)
                r4 = 10
                java.lang.Integer r5 = java.lang.Integer.valueOf(r3, r4)     // Catch:{ Exception -> 0x005b }
                int r5 = r5.intValue()     // Catch:{ Exception -> 0x005b }
                goto L_0x0060
            L_0x005b:
                r5 = move-exception
                p002eu.chainfire.librootjava.C0078d.m287a(r5)
                r5 = 0
            L_0x0060:
                java.lang.String r6 = "VERSION"
                java.lang.String r7 = "%s %d"
                r8 = 2
                java.lang.Object[] r9 = new java.lang.Object[r8]
                r9[r1] = r2
                java.lang.Integer r10 = java.lang.Integer.valueOf(r5)
                r9[r14] = r10
                p002eu.chainfire.librootjava.C0078d.m289a(r6, r7, r9)
                boolean r0 = r0[r1]
                if (r0 != 0) goto L_0x007b
                java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
                return r14
            L_0x007b:
                if (r2 == 0) goto L_0x0088
                java.lang.String r0 = ":SUPERSU"
                boolean r0 = r2.endsWith(r0)
                if (r0 != 0) goto L_0x0086
                goto L_0x0088
            L_0x0086:
                r0 = 1
                goto L_0x0089
            L_0x0088:
                r0 = 0
            L_0x0089:
                if (r0 == 0) goto L_0x0093
                if (r3 == 0) goto L_0x0091
                r2 = 240(0xf0, float:3.36E-43)
                if (r5 >= r2) goto L_0x0093
            L_0x0091:
                r2 = 0
                goto L_0x0094
            L_0x0093:
                r2 = r0
            L_0x0094:
                java.lang.String r3 = "ls -ld /system/etc/init.d /su/su.d /sbin/supersu/su.d /data/adb/post-fs-data.d /sbin/.core/img/.core/post-fs-data.d 2> /dev/null"
                java.util.List r3 = p002eu.chainfire.p005b.C0032c.C0063q.m241a(r3)
                if (r3 == 0) goto L_0x00df
                java.util.Iterator r3 = r3.iterator()
                r5 = 0
                r6 = 0
                r7 = 0
                r9 = 0
                r10 = 0
            L_0x00a5:
                boolean r11 = r3.hasNext()
                if (r11 == 0) goto L_0x00e4
                java.lang.Object r11 = r3.next()
                java.lang.String r11 = (java.lang.String) r11
                java.lang.String r12 = "init.d"
                boolean r12 = r11.contains(r12)
                if (r12 == 0) goto L_0x00ba
                r5 = 1
            L_0x00ba:
                java.lang.String r12 = "su.d"
                boolean r12 = r11.contains(r12)
                if (r12 == 0) goto L_0x00c3
                r10 = 1
            L_0x00c3:
                java.lang.String r12 = "su.d"
                boolean r12 = r11.contains(r12)
                if (r12 == 0) goto L_0x00cc
                r9 = 1
            L_0x00cc:
                java.lang.String r12 = "/adb/post-fs-data.d"
                boolean r12 = r11.contains(r12)
                if (r12 == 0) goto L_0x00d5
                r7 = 1
            L_0x00d5:
                java.lang.String r12 = "/.core/post-fs-data.d"
                boolean r11 = r11.contains(r12)
                if (r11 == 0) goto L_0x00a5
                r6 = 1
                goto L_0x00a5
            L_0x00df:
                r5 = 0
                r6 = 0
                r7 = 0
                r9 = 0
                r10 = 0
            L_0x00e4:
                if (r0 == 0) goto L_0x00ef
                if (r2 != 0) goto L_0x00ef
                if (r5 != 0) goto L_0x00ef
                java.lang.Integer r14 = java.lang.Integer.valueOf(r8)
                return r14
            L_0x00ef:
                if (r0 != 0) goto L_0x00fd
                if (r5 != 0) goto L_0x00fd
                if (r6 != 0) goto L_0x00fd
                if (r7 != 0) goto L_0x00fd
                r14 = 3
            L_0x00f8:
                java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
                return r14
            L_0x00fd:
                if (r7 == 0) goto L_0x0107
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r2 = p002eu.chainfire.liveboot.C0144b.C0147b.MAGISK_ADB
            L_0x0103:
                r0.f289k = r2
                goto L_0x0125
            L_0x0107:
                if (r6 == 0) goto L_0x010e
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r2 = p002eu.chainfire.liveboot.C0144b.C0147b.MAGISK_CORE
                goto L_0x0103
            L_0x010e:
                if (r9 == 0) goto L_0x0115
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r2 = p002eu.chainfire.liveboot.C0144b.C0147b.SBIN_SU_D
                goto L_0x0103
            L_0x0115:
                if (r10 == 0) goto L_0x011c
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r2 = p002eu.chainfire.liveboot.C0144b.C0147b.SU_SU_D
                goto L_0x0103
            L_0x011c:
                if (r2 != 0) goto L_0x0125
                if (r5 == 0) goto L_0x0125
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r2 = p002eu.chainfire.liveboot.C0144b.C0147b.INIT_D
                goto L_0x0103
            L_0x0125:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.a r0 = r0.f286h
                boolean r0 = r0.mo233a()
                if (r0 == 0) goto L_0x01d0
                r0 = 0
            L_0x0132:
                if (r0 >= r4) goto L_0x0149
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.a r2 = r2.f286h
                boolean r2 = r2.mo236b()
                if (r2 == 0) goto L_0x0141
                goto L_0x0149
            L_0x0141:
                r2 = 64
                java.lang.Thread.sleep(r2)     // Catch:{ Exception -> 0x0146 }
            L_0x0146:
                int r0 = r0 + 1
                goto L_0x0132
            L_0x0149:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.a r0 = r0.f286h
                boolean r0 = r0.mo236b()
                if (r0 == 0) goto L_0x0173
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.a r0 = r0.f286h
                r0.mo237c()
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.a r0 = r0.f286h
                r2 = 0
                eu.chainfire.liveboot.a$c[] r0 = r0.mo235a(r2, r1)
                if (r0 == 0) goto L_0x0173
                int r0 = r0.length
                if (r0 <= 0) goto L_0x0173
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                r0.f288j = r14
            L_0x0173:
                android.app.Activity r0 = r13.f323b
                android.content.pm.PackageManager r0 = r0.getPackageManager()
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this
                boolean r2 = r2.f288j
                if (r2 != 0) goto L_0x0193
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this     // Catch:{ NameNotFoundException -> 0x0192 }
                java.lang.String r3 = "eu.chainfire.livedmesg"
                android.content.pm.PackageInfo r3 = r0.getPackageInfo(r3, r1)     // Catch:{ NameNotFoundException -> 0x0192 }
                if (r3 == 0) goto L_0x018d
                r3 = 1
                goto L_0x018e
            L_0x018d:
                r3 = 0
            L_0x018e:
                r2.f288j = r3     // Catch:{ NameNotFoundException -> 0x0192 }
                goto L_0x0193
            L_0x0192:
            L_0x0193:
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this
                boolean r2 = r2.f288j
                if (r2 != 0) goto L_0x01ad
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this     // Catch:{ NameNotFoundException -> 0x01ac }
                java.lang.String r3 = "eu.chainfire.livelogcat"
                android.content.pm.PackageInfo r0 = r0.getPackageInfo(r3, r1)     // Catch:{ NameNotFoundException -> 0x01ac }
                if (r0 == 0) goto L_0x01a7
                r0 = 1
                goto L_0x01a8
            L_0x01a7:
                r0 = 0
            L_0x01a8:
                r2.f288j = r0     // Catch:{ NameNotFoundException -> 0x01ac }
                goto L_0x01ad
            L_0x01ac:
            L_0x01ad:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                boolean r0 = r0.f288j
                if (r0 != 0) goto L_0x01c4
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.d r0 = r0.f281c
                eu.chainfire.liveboot.d$a r0 = r0.f479o
                boolean r0 = r0.mo266a()
                if (r0 == 0) goto L_0x01d5
                goto L_0x01d0
            L_0x01c4:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.d r0 = r0.f281c
                eu.chainfire.liveboot.d$a r0 = r0.f479o
                r0.mo265a(r14)
                goto L_0x01d5
            L_0x01d0:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                r0.f288j = r14
            L_0x01d5:
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.SettingsFragment r2 = p002eu.chainfire.liveboot.SettingsFragment.this
                boolean r2 = r2.f288j
                r0.f287i = r2
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                boolean r0 = r0.f287i
                if (r0 != 0) goto L_0x01fb
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.d r0 = r0.f281c
                eu.chainfire.liveboot.d$a r0 = r0.f480p
                boolean r0 = r0.mo266a()
                if (r0 == 0) goto L_0x01fb
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                r0.f287i = r14
            L_0x01fb:
                android.app.Activity r14 = r13.f323b
                eu.chainfire.liveboot.SettingsFragment r0 = p002eu.chainfire.liveboot.SettingsFragment.this
                eu.chainfire.liveboot.b$b r0 = r0.f289k
                boolean r14 = p002eu.chainfire.liveboot.C0144b.m441b(r14, r0)
                if (r14 == 0) goto L_0x020c
                r14 = 4
                goto L_0x00f8
            L_0x020c:
                java.lang.Integer r14 = java.lang.Integer.valueOf(r1)
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.SettingsFragment.C0111a.doInBackground(java.lang.Void[]):java.lang.Integer");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            SettingsFragment settingsFragment;
            Activity activity;
            int i;
            int i2;
            Runnable r6;
            SettingsFragment.this.m339a(true);
            try {
                this.f324c.dismiss();
                if (num.intValue() == 0) {
                    m349a();
                    return;
                }
                if (num.intValue() == 1) {
                    settingsFragment = SettingsFragment.this;
                    activity = this.f323b;
                    i = R.string.error_not_rooted;
                    i2 = R.string.generic_ok;
                    r6 = new Runnable() {
                        public void run() {
                            C0111a.this.m351b();
                        }
                    };
                } else if (num.intValue() == 2) {
                    settingsFragment = SettingsFragment.this;
                    activity = this.f323b;
                    i = R.string.error_supersu_old;
                    i2 = R.string.generic_ok;
                    r6 = new Runnable() {
                        public void run() {
                            C0111a.this.m351b();
                        }
                    };
                } else if (num.intValue() == 3) {
                    SettingsFragment.this.m334a(this.f323b, R.string.error_no_supersu_nor_initd, R.string.generic_ok, new Runnable() {
                        public void run() {
                            C0111a.this.m351b();
                        }
                    }, 0, null, 0, null);
                    return;
                } else if (num.intValue() != 4) {
                    return;
                } else {
                    if (SettingsFragment.this.f289k != C0147b.SU_D && SettingsFragment.this.f289k != C0147b.INIT_D) {
                        C0144b.m433a(this.f323b, SettingsFragment.this.f289k, new Runnable() {
                            public void run() {
                                C0111a.this.m349a();
                            }
                        });
                        return;
                    } else if (!C0144b.m435a(1024, 2)) {
                        SettingsFragment.this.m334a(this.f323b, R.string.error_install_nospace, R.string.generic_close, new Runnable() {
                            public void run() {
                                C0111a.this.m351b();
                            }
                        }, 0, null, 0, null);
                        return;
                    } else {
                        SettingsFragment.this.m334a(this.f323b, R.string.error_install_needed, R.string.generic_cancel, new Runnable() {
                            public void run() {
                                C0111a.this.m351b();
                            }
                        }, 0, null, R.string.generic_install, new Runnable() {
                            public void run() {
                                C0144b.m433a(C0111a.this.f323b, SettingsFragment.this.f289k, new Runnable() {
                                    public void run() {
                                        C0111a.this.m349a();
                                    }
                                });
                            }
                        });
                        return;
                    }
                }
                settingsFragment.m334a(activity, i, i2, r6, 0, null, 0, null);
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SettingsFragment.this.m339a(false);
            this.f324c = new ProgressDialog(this.f323b);
            this.f324c.setMessage(this.f323b.getString(R.string.loading));
            this.f324c.setIndeterminate(true);
            this.f324c.setProgressStyle(0);
            this.f324c.setCancelable(false);
            this.f324c.show();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0203 A[LOOP:2: B:43:0x01fe->B:45:0x0203, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x03a0  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x03fc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x040c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0420  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.preference.PreferenceScreen m331a() {
        /*
            r21 = this;
            r9 = r21
            android.app.Activity r15 = r21.getActivity()
            r14 = 0
            if (r15 != 0) goto L_0x000a
            return r14
        L_0x000a:
            android.preference.PreferenceManager r0 = r21.getPreferenceManager()
            android.preference.PreferenceScreen r13 = r0.createPreferenceScreen(r15)
            r0 = 2131361793(0x7f0a0001, float:1.8343348E38)
            java.lang.String r0 = r15.getString(r0)
            r9.f279a = r0
            java.lang.String r0 = r9.f279a
            boolean r1 = r9.f288j
            if (r1 == 0) goto L_0x0033
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " Pro"
        L_0x002b:
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x0042
        L_0x0033:
            boolean r1 = r9.f287i
            if (r1 == 0) goto L_0x0042
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r0 = " PseudoPro"
            goto L_0x002b
        L_0x0042:
            r10 = 0
            android.content.pm.PackageManager r1 = r15.getPackageManager()     // Catch:{ Exception -> 0x0066 }
            java.lang.String r2 = r15.getPackageName()     // Catch:{ Exception -> 0x0066 }
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r2, r10)     // Catch:{ Exception -> 0x0066 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0066 }
            r2.<init>()     // Catch:{ Exception -> 0x0066 }
            r2.append(r0)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r3 = " v"
            r2.append(r3)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r1 = r1.versionName     // Catch:{ Exception -> 0x0066 }
            r2.append(r1)     // Catch:{ Exception -> 0x0066 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0066 }
            r0 = r1
        L_0x0066:
            android.preference.Preference r1 = new android.preference.Preference
            r1.<init>(r15)
            r1.setTitle(r0)
            r0 = 2131361792(0x7f0a0000, float:1.8343346E38)
            r1.setSummary(r0)
            java.lang.String r0 = "copyright"
            r1.setKey(r0)
            r12 = 1
            r1.setEnabled(r12)
            eu.chainfire.liveboot.SettingsFragment$15 r0 = new eu.chainfire.liveboot.SettingsFragment$15
            r0.<init>()
            r1.setOnPreferenceClickListener(r0)
            r13.addPreference(r1)
            boolean r0 = r9.f288j
            if (r0 != 0) goto L_0x00a3
            android.app.Activity r1 = r21.getActivity()
            r2 = 0
            r3 = 2131361851(0x7f0a003b, float:1.8343466E38)
            r4 = 2131361850(0x7f0a003a, float:1.8343464E38)
            r5 = 1
            eu.chainfire.liveboot.SettingsFragment$16 r6 = new eu.chainfire.liveboot.SettingsFragment$16
            r6.<init>()
            android.preference.Preference r0 = p002eu.chainfire.liveboot.C0148c.m453a(r1, r2, r3, r4, r5, r6)
            r13.addPreference(r0)
        L_0x00a3:
            r0 = 2131361842(0x7f0a0032, float:1.8343448E38)
            android.preference.PreferenceCategory r11 = p002eu.chainfire.liveboot.C0148c.m454a(r15, r13, r0)
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r8 = 7
            java.lang.CharSequence[] r1 = new java.lang.CharSequence[r8]
            java.lang.CharSequence[] r2 = new java.lang.CharSequence[r8]
            eu.chainfire.liveboot.d r3 = r9.f281c
            eu.chainfire.liveboot.d$d r3 = r3.f471g
            java.lang.String r3 = r3.mo271a()
            r4 = 0
        L_0x00bd:
            r7 = 6
            if (r4 > r7) goto L_0x00ef
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = ""
            r5.append(r6)
            char[] r6 = p002eu.chainfire.liveboot.p006a.C0131b.f383a
            char r6 = r6[r4]
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            int r6 = r4 + 0
            int[] r7 = p002eu.chainfire.liveboot.p006a.C0131b.f385c
            r7 = r7[r4]
            java.lang.String r7 = r15.getString(r7)
            r1[r6] = r7
            r2[r6] = r5
            boolean r6 = r3.contains(r5)
            if (r6 == 0) goto L_0x00ec
            r0.add(r5)
        L_0x00ec:
            int r4 = r4 + 1
            goto L_0x00bd
        L_0x00ef:
            android.preference.MultiSelectListPreference r3 = new android.preference.MultiSelectListPreference
            r3.<init>(r15)
            r9.f282d = r3
            android.preference.MultiSelectListPreference r3 = r9.f282d
            r4 = 2131361863(0x7f0a0047, float:1.834349E38)
            r3.setTitle(r4)
            android.preference.MultiSelectListPreference r3 = r9.f282d
            r3.setDialogTitle(r4)
            android.preference.MultiSelectListPreference r3 = r9.f282d
            r3.setPersistent(r10)
            android.preference.MultiSelectListPreference r3 = r9.f282d
            r3.setEntries(r1)
            android.preference.MultiSelectListPreference r1 = r9.f282d
            r1.setEntryValues(r2)
            android.preference.MultiSelectListPreference r1 = r9.f282d
            r1.setValues(r0)
            android.preference.MultiSelectListPreference r0 = r9.f282d
            eu.chainfire.liveboot.SettingsFragment$17 r1 = new eu.chainfire.liveboot.SettingsFragment$17
            r1.<init>()
            r0.setOnPreferenceChangeListener(r1)
            android.preference.MultiSelectListPreference r0 = r9.f282d
            r11.addPreference(r0)
            boolean r0 = r9.f287i
            if (r0 != 0) goto L_0x014b
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f472h
            java.lang.String r0 = r0.mo271a()
            eu.chainfire.liveboot.d r1 = r9.f281c
            eu.chainfire.liveboot.d$d r1 = r1.f472h
            java.lang.String r1 = r1.f489a
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x014b
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f472h
            eu.chainfire.liveboot.d r1 = r9.f281c
            eu.chainfire.liveboot.d$d r1 = r1.f472h
            java.lang.String r1 = r1.f489a
            r0.mo272a(r1)
        L_0x014b:
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r6 = 5
            java.lang.CharSequence[] r1 = new java.lang.CharSequence[r6]
            java.lang.CharSequence[] r2 = new java.lang.CharSequence[r6]
            eu.chainfire.liveboot.d r3 = r9.f281c
            eu.chainfire.liveboot.d$d r3 = r3.f472h
            java.lang.String r3 = r3.mo271a()
            r4 = 0
        L_0x015e:
            r5 = 4
            if (r4 > r5) goto L_0x0192
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = ""
            r5.append(r6)
            char[] r6 = p002eu.chainfire.liveboot.p006a.C0131b.f386d
            char r6 = r6[r4]
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            int r6 = r4 + 0
            int[] r16 = p002eu.chainfire.liveboot.p006a.C0131b.f387e
            r7 = r16[r4]
            java.lang.String r7 = r15.getString(r7)
            r1[r6] = r7
            r2[r6] = r5
            boolean r6 = r3.contains(r5)
            if (r6 == 0) goto L_0x018d
            r0.add(r5)
        L_0x018d:
            int r4 = r4 + 1
            r6 = 5
            r7 = 6
            goto L_0x015e
        L_0x0192:
            android.preference.MultiSelectListPreference r3 = new android.preference.MultiSelectListPreference
            r3.<init>(r15)
            r9.f283e = r3
            android.preference.MultiSelectListPreference r3 = r9.f283e
            r4 = 2131361857(0x7f0a0041, float:1.8343478E38)
            r3.setTitle(r4)
            android.preference.MultiSelectListPreference r3 = r9.f283e
            r3.setDialogTitle(r4)
            android.preference.MultiSelectListPreference r3 = r9.f283e
            r3.setPersistent(r10)
            android.preference.MultiSelectListPreference r3 = r9.f283e
            r3.setEntries(r1)
            android.preference.MultiSelectListPreference r1 = r9.f283e
            r1.setEntryValues(r2)
            android.preference.MultiSelectListPreference r1 = r9.f283e
            r1.setValues(r0)
            android.preference.MultiSelectListPreference r0 = r9.f283e
            eu.chainfire.liveboot.SettingsFragment$18 r1 = new eu.chainfire.liveboot.SettingsFragment$18
            r1.<init>()
            r0.setOnPreferenceChangeListener(r1)
            android.preference.MultiSelectListPreference r0 = r9.f283e
            r11.addPreference(r0)
            android.preference.MultiSelectListPreference r0 = r9.f283e
            r9.m335a(r15, r0)
            boolean r0 = r9.f287i
            if (r0 != 0) goto L_0x01f3
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f473i
            java.lang.String r0 = r0.mo271a()
            eu.chainfire.liveboot.d r1 = r9.f281c
            eu.chainfire.liveboot.d$d r1 = r1.f473i
            java.lang.String r1 = r1.f489a
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x01f3
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f473i
            eu.chainfire.liveboot.d r1 = r9.f281c
            eu.chainfire.liveboot.d$d r1 = r1.f473i
            java.lang.String r1 = r1.f489a
            r0.mo272a(r1)
        L_0x01f3:
            java.lang.String[] r0 = p002eu.chainfire.liveboot.p006a.C0131b.f390h
            int r0 = r0.length
            java.lang.CharSequence[] r7 = new java.lang.CharSequence[r0]
            java.lang.String[] r0 = p002eu.chainfire.liveboot.p006a.C0131b.f390h
            int r0 = r0.length
            java.lang.CharSequence[] r6 = new java.lang.CharSequence[r0]
            r0 = 0
        L_0x01fe:
            java.lang.String[] r1 = p002eu.chainfire.liveboot.p006a.C0131b.f390h
            int r1 = r1.length
            if (r0 >= r1) goto L_0x0216
            int[] r1 = p002eu.chainfire.liveboot.p006a.C0131b.f389g
            r1 = r1[r0]
            java.lang.String r1 = r9.getString(r1)
            r7[r0] = r1
            java.lang.String[] r1 = p002eu.chainfire.liveboot.p006a.C0131b.f390h
            r1 = r1[r0]
            r6[r0] = r1
            int r0 = r0 + 1
            goto L_0x01fe
        L_0x0216:
            r2 = 2131361861(0x7f0a0045, float:1.8343486E38)
            r3 = 2131361860(0x7f0a0044, float:1.8343484E38)
            r4 = 2131361861(0x7f0a0045, float:1.8343486E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f473i
            java.lang.String r1 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$d r0 = r0.f473i
            java.lang.String r0 = r0.f489a
            r16 = r0
            r0 = r15
            r17 = r1
            r1 = r11
            r18 = 4
            r5 = r17
            r17 = r6
            r19 = 5
            r6 = r16
            r16 = 6
            r20 = 7
            r8 = r17
            android.preference.ListPreference r0 = p002eu.chainfire.liveboot.C0148c.m451a(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            r9.f284f = r0
            android.preference.ListPreference r0 = r9.f284f
            r9.m335a(r15, r0)
            r2 = 2131361859(0x7f0a0043, float:1.8343482E38)
            r3 = 2131361858(0x7f0a0042, float:1.834348E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f474j
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f474j
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r15
            p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r0 = 2131361841(0x7f0a0031, float:1.8343446E38)
            android.preference.PreferenceCategory r1 = p002eu.chainfire.liveboot.C0148c.m454a(r15, r13, r0)
            r2 = 2131361849(0x7f0a0039, float:1.8343462E38)
            r3 = 2131361848(0x7f0a0038, float:1.834346E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f475k
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f475k
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r15
            p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r0 = 2131361845(0x7f0a0035, float:1.8343454E38)
            android.preference.PreferenceCategory r6 = p002eu.chainfire.liveboot.C0148c.m454a(r15, r13, r0)
            boolean r0 = r9.f287i
            if (r0 != 0) goto L_0x02a3
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f469e
            boolean r0 = r0.mo266a()
            if (r0 == 0) goto L_0x02a3
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f469e
            r0.mo265a(r10)
        L_0x02a3:
            r2 = 2131361874(0x7f0a0052, float:1.8343513E38)
            r3 = 2131361873(0x7f0a0051, float:1.834351E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f469e
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f469e
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r15
            r1 = r6
            android.preference.CheckBoxPreference r0 = p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r9.m335a(r15, r0)
            r2 = 2131361847(0x7f0a0037, float:1.8343458E38)
            r3 = 2131361846(0x7f0a0036, float:1.8343456E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f470f
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f470f
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r15
            p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r0 = 8
            java.lang.CharSequence[] r0 = new java.lang.CharSequence[r0]
            java.lang.String r1 = "20"
            r0[r10] = r1
            java.lang.String r1 = "40"
            r0[r12] = r1
            r1 = 2
            java.lang.String r2 = "60"
            r0[r1] = r2
            r1 = 3
            java.lang.String r2 = "80"
            r0[r1] = r2
            java.lang.String r1 = "100"
            r0[r18] = r1
            java.lang.String r1 = "120"
            r0[r19] = r1
            java.lang.String r1 = "140"
            r0[r16] = r1
            java.lang.String r1 = "160"
            r0[r20] = r1
            r1 = 2131361855(0x7f0a003f, float:1.8343474E38)
            r2 = 0
            r3 = 2131361855(0x7f0a003f, float:1.8343474E38)
            eu.chainfire.liveboot.d r4 = r9.f281c
            eu.chainfire.liveboot.d$d r4 = r4.f476l
            java.lang.String r4 = r4.f487e
            eu.chainfire.liveboot.d r5 = r9.f281c
            eu.chainfire.liveboot.d$d r5 = r5.f476l
            java.lang.String r5 = r5.f489a
            r19 = 1
            r10 = r15
            r11 = r6
            r7 = 1
            r12 = r1
            r8 = r13
            r13 = r2
            r2 = r14
            r14 = r3
            r3 = r15
            r15 = r4
            r16 = r5
            r17 = r0
            r18 = r0
            android.preference.ListPreference r0 = p002eu.chainfire.liveboot.C0148c.m452a(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            r9.f285g = r0
            r4 = 2131361878(0x7f0a0056, float:1.834352E38)
            r5 = 2131361877(0x7f0a0055, float:1.8343519E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f477m
            java.lang.String r10 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f477m
            boolean r0 = r0.f481a
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r0)
            r0 = r3
            r1 = r6
            r12 = r2
            r2 = r4
            r13 = r3
            r3 = r5
            r4 = r10
            r5 = r11
            p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r2 = 2131361870(0x7f0a004e, float:1.8343505E38)
            r3 = 2131361869(0x7f0a004d, float:1.8343503E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f478n
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f478n
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r13
            p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            r0 = 2131361844(0x7f0a0034, float:1.8343452E38)
            android.preference.PreferenceCategory r6 = p002eu.chainfire.liveboot.C0148c.m454a(r13, r8, r0)
            r2 = 2131361872(0x7f0a0050, float:1.8343509E38)
            r3 = 2131361871(0x7f0a004f, float:1.8343507E38)
            r4 = 1
            eu.chainfire.liveboot.SettingsFragment$19 r5 = new eu.chainfire.liveboot.SettingsFragment$19
            r5.<init>(r13)
            r0 = r13
            r1 = r6
            p002eu.chainfire.liveboot.C0148c.m453a(r0, r1, r2, r3, r4, r5)
            r2 = 2131361876(0x7f0a0054, float:1.8343517E38)
            r3 = 2131361875(0x7f0a0053, float:1.8343515E38)
            eu.chainfire.liveboot.SettingsFragment$2 r5 = new eu.chainfire.liveboot.SettingsFragment$2
            r5.<init>(r13)
            p002eu.chainfire.liveboot.C0148c.m453a(r0, r1, r2, r3, r4, r5)
            r2 = 2131361868(0x7f0a004c, float:1.83435E38)
            r3 = 2131361867(0x7f0a004b, float:1.8343498E38)
            eu.chainfire.liveboot.SettingsFragment$3 r5 = new eu.chainfire.liveboot.SettingsFragment$3
            r5.<init>(r13)
            p002eu.chainfire.liveboot.C0148c.m453a(r0, r1, r2, r3, r4, r5)
            boolean r0 = r9.f288j
            if (r0 != 0) goto L_0x03c4
            r2 = 2131361853(0x7f0a003d, float:1.834347E38)
            r3 = 2131361852(0x7f0a003c, float:1.8343468E38)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f480p
            java.lang.String r4 = r0.f487e
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f480p
            boolean r0 = r0.f481a
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r0 = r13
            r1 = r6
            android.preference.CheckBoxPreference r0 = p002eu.chainfire.liveboot.C0148c.m449a(r0, r1, r2, r3, r4, r5)
            eu.chainfire.liveboot.SettingsFragment$4 r1 = new eu.chainfire.liveboot.SettingsFragment$4
            r1.<init>(r13)
            r0.setOnPreferenceChangeListener(r1)
        L_0x03c4:
            r0 = 2131361843(0x7f0a0033, float:1.834345E38)
            android.preference.PreferenceCategory r6 = p002eu.chainfire.liveboot.C0148c.m454a(r13, r8, r0)
            r2 = 2131361864(0x7f0a0048, float:1.8343492E38)
            r3 = 2131361865(0x7f0a0049, float:1.8343494E38)
            r4 = 1
            eu.chainfire.liveboot.SettingsFragment$5 r5 = new eu.chainfire.liveboot.SettingsFragment$5
            r5.<init>()
            r0 = r13
            r1 = r6
            p002eu.chainfire.liveboot.C0148c.m453a(r0, r1, r2, r3, r4, r5)
            r2 = 2131361806(0x7f0a000e, float:1.8343375E38)
            r3 = 2131361805(0x7f0a000d, float:1.8343373E38)
            eu.chainfire.liveboot.SettingsFragment$6 r5 = new eu.chainfire.liveboot.SettingsFragment$6
            r5.<init>()
            p002eu.chainfire.liveboot.C0148c.m453a(r0, r1, r2, r3, r4, r5)
            r9.m338a(r12)
            android.content.SharedPreferences r0 = r9.f280b
            r0.registerOnSharedPreferenceChangeListener(r9)
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f467c
            boolean r0 = r0.mo266a()
            if (r0 != 0) goto L_0x0406
            eu.chainfire.liveboot.d r0 = r9.f281c
            eu.chainfire.liveboot.d$a r0 = r0.f467c
            r0.mo265a(r7)
            r9.m340b(r7)
        L_0x0406:
            eu.chainfire.liveboot.b$b r0 = r9.f289k
            eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.INIT_D
            if (r0 != r1) goto L_0x0420
            r2 = 2131361881(0x7f0a0059, float:1.8343527E38)
            r3 = 0
            r4 = 0
            r5 = 2131361815(0x7f0a0017, float:1.8343393E38)
            r6 = 0
            r7 = 0
            r10 = 0
            r0 = r21
            r1 = r13
            r11 = r8
            r8 = r10
            r0.m334a(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0421
        L_0x0420:
            r11 = r8
        L_0x0421:
            android.content.Context r0 = p002eu.chainfire.liveboot.C0144b.m430a(r13)     // Catch:{ Exception -> 0x042c }
            java.io.File r0 = r0.getFilesDir()     // Catch:{ Exception -> 0x042c }
            r0.mkdirs()     // Catch:{ Exception -> 0x042c }
        L_0x042c:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.SettingsFragment.m331a():android.preference.PreferenceScreen");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m334a(Activity activity, int i, int i2, final Runnable runnable, int i3, final Runnable runnable2, int i4, final Runnable runnable3) {
        Builder onCancelListener = new Builder(activity).setTitle(R.string.app_name).setMessage(i).setCancelable(true).setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        if (i2 > 0) {
            onCancelListener.setNegativeButton(i2, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (runnable != null) {
                        runnable.run();
                    }
                }
            });
        }
        if (i3 > 0) {
            onCancelListener.setNeutralButton(i3, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (runnable2 != null) {
                        runnable2.run();
                    }
                }
            });
        }
        if (i4 > 0) {
            onCancelListener.setPositiveButton(i4, new OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (runnable3 != null) {
                        runnable3.run();
                    }
                }
            });
        }
        onCancelListener.show();
    }

    /* renamed from: a */
    private void m335a(Activity activity, Preference preference) {
        if (!this.f287i) {
            preference.setEnabled(false);
            preference.setSummary(String.format("%s\n%s", new Object[]{(String) preference.getSummary(), activity.getString(R.string.pro_required)}));
        }
    }

    /* renamed from: a */
    private void m338a(String str) {
        String str2;
        String str3;
        final Activity activity = getActivity();
        if ((str == null || str.equals(this.f281c.f471g.f487e)) && this.f282d != null) {
            String a = this.f281c.f471g.mo271a();
            if (a.length() == 0) {
                str3 = getString(R.string.generic_none);
            } else if (a.length() == 7) {
                str3 = getString(R.string.generic_all);
            } else {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i <= 6; i++) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("");
                    sb2.append(C0131b.f383a[i]);
                    if (a.contains(sb2.toString())) {
                        sb.append(getString(C0131b.f385c[i]));
                        sb.append(", ");
                    }
                }
                String sb3 = sb.toString();
                str3 = sb3.substring(0, sb3.length() - 2);
            }
            this.f282d.setSummary(String.format(Locale.ENGLISH, "%s\n[ %s ]", new Object[]{getString(R.string.settings_logcat_levels_description), str3}));
        }
        if ((str == null || str.equals(this.f281c.f472h.f487e)) && this.f283e != null) {
            String a2 = this.f281c.f472h.mo271a();
            if (a2.length() == 0) {
                str2 = getString(R.string.generic_none);
            } else if (a2.length() == 7) {
                str2 = getString(R.string.generic_all);
            } else {
                StringBuilder sb4 = new StringBuilder();
                for (int i2 = 0; i2 <= 4; i2++) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("");
                    sb5.append(C0131b.f386d[i2]);
                    if (a2.contains(sb5.toString())) {
                        sb4.append(getString(C0131b.f387e[i2]));
                        sb4.append(", ");
                    }
                }
                String sb6 = sb4.toString();
                str2 = sb6.substring(0, sb6.length() - 2);
            }
            this.f283e.setSummary(String.format(Locale.ENGLISH, "%s\n[ %s ]", new Object[]{getString(R.string.settings_logcat_buffers_description), str2}));
            if (activity != null) {
                m335a(activity, (Preference) this.f283e);
            }
        }
        if (str == null || str.equals(this.f281c.f473i.f487e)) {
            if (this.f284f != null) {
                String str4 = null;
                String a3 = this.f281c.f473i.mo271a();
                int i3 = 0;
                while (true) {
                    if (i3 >= C0131b.f390h.length) {
                        break;
                    } else if (C0131b.f390h[i3].equals(a3)) {
                        str4 = getString(C0131b.f389g[i3]);
                        break;
                    } else {
                        i3++;
                    }
                }
                this.f284f.setSummary(String.format(Locale.ENGLISH, "%s\n[ %s ]", new Object[]{getString(R.string.settings_logcat_format_description), str4}));
            }
            if (activity != null) {
                m335a(activity, (Preference) this.f284f);
            }
        }
        if ((str == null || str.equals(this.f281c.f476l.f487e)) && this.f285g != null) {
            this.f285g.setSummary(String.format(Locale.ENGLISH, "%s\n[ %s ]", new Object[]{getString(R.string.settings_lines_description), this.f281c.f476l.mo271a()}));
        }
        if (str != null && activity != null) {
            new Thread(new Runnable() {
                public void run() {
                    C0144b.m442c(activity);
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m339a(boolean z) {
        Activity activity = getActivity();
        if (activity != null && (activity instanceof MainActivity)) {
            ((MainActivity) activity).mo187a(z);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m340b(boolean z) {
        Builder cancelable;
        int i;
        C01098 r1;
        Activity activity = getActivity();
        if (activity != null) {
            if (z) {
                cancelable = new Builder(activity).setTitle(R.string.follow_popup_title).setMessage(R.string.follow_popup_desc).setCancelable(true).setPositiveButton(R.string.follow_twitter, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse("http://www.twitter.com/ChainfireXDA"));
                        SettingsFragment.this.startActivity(intent);
                    }
                }).setNeutralButton(R.string.follow_gplus, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse("http://google.com/+Chainfire"));
                        SettingsFragment.this.startActivity(intent);
                    }
                });
                i = R.string.follow_nothanks;
                r1 = new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                };
            } else {
                cancelable = new Builder(activity).setTitle(R.string.follow_popup_title).setItems(new CharSequence[]{getString(R.string.follow_twitter), getString(R.string.follow_gplus)}, new OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent;
                        String str;
                        if (i == 0) {
                            intent = new Intent("android.intent.action.VIEW");
                            str = "http://www.twitter.com/ChainfireXDA";
                        } else if (i == 1) {
                            intent = new Intent("android.intent.action.VIEW");
                            str = "http://google.com/+Chainfire";
                        } else {
                            return;
                        }
                        intent.setData(Uri.parse(str));
                        SettingsFragment.this.startActivity(intent);
                    }
                }).setCancelable(true);
                i = R.string.generic_close;
                r1 = null;
            }
            try {
                cancelable.setNegativeButton(i, r1).show();
            } catch (Exception unused) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f281c = C0149d.m455a(getActivity());
        this.f280b = this.f281c.mo264a();
        this.f286h = new C0121a(getActivity());
        new C0111a(getActivity()).execute(new Void[0]);
    }

    public void onDestroy() {
        try {
            this.f286h.close();
        } catch (Exception unused) {
        }
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        try {
            m338a(str);
        } catch (Throwable unused) {
        }
    }
}
