package p002eu.chainfire.liveboot;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/* renamed from: eu.chainfire.liveboot.d */
public class C0149d {

    /* renamed from: q */
    private static C0149d f464q;

    /* renamed from: a */
    protected SharedPreferences f465a = null;

    /* renamed from: b */
    protected Editor f466b = null;

    /* renamed from: c */
    public C0150a f467c = new C0150a(this, "shown_follow", false);

    /* renamed from: d */
    public C0151b f468d = new C0151b(this, "last_update", 0);

    /* renamed from: e */
    public C0150a f469e = new C0150a(this, "transparent", false);

    /* renamed from: f */
    public C0150a f470f = new C0150a(this, "dark", false);

    /* renamed from: g */
    public C0153d f471g = new C0153d(this, "logcat", "VDIWEFS");

    /* renamed from: h */
    public C0153d f472h = new C0153d(this, "logcat_buffers", "MSC");

    /* renamed from: i */
    public C0153d f473i = new C0153d(this, "logcat_format", "brief");

    /* renamed from: j */
    public C0150a f474j = new C0150a(this, "logcat_colors", true);

    /* renamed from: k */
    public C0150a f475k = new C0150a(this, "dmesg", true);

    /* renamed from: l */
    public C0153d f476l = new C0153d(this, "lines", "80");

    /* renamed from: m */
    public C0150a f477m = new C0150a(this, "word_wrap", true);

    /* renamed from: n */
    public C0150a f478n = new C0150a(this, "save_logs", false);

    /* renamed from: o */
    public C0150a f479o = new C0150a(this, "have_pro_cached", false);

    /* renamed from: p */
    public C0150a f480p = new C0150a(this, "freeload", false);

    /* renamed from: eu.chainfire.liveboot.d$a */
    public class C0150a extends C0152c {

        /* renamed from: a */
        public boolean f481a = false;

        public C0150a(C0149d dVar, String str, boolean z) {
            super(dVar, str);
            this.f481a = z;
        }

        /* renamed from: a */
        public void mo265a(boolean z) {
            mo269b().putBoolean(this.f487e, z);
            mo270c();
        }

        /* renamed from: a */
        public boolean mo266a() {
            return this.f485c.f465a.getBoolean(this.f487e, this.f481a);
        }
    }

    /* renamed from: eu.chainfire.liveboot.d$b */
    public class C0151b extends C0152c {

        /* renamed from: a */
        public int f483a = 0;

        public C0151b(C0149d dVar, String str, int i) {
            super(dVar, str);
            this.f483a = i;
        }

        /* renamed from: a */
        public int mo267a() {
            return this.f485c.f465a.getInt(this.f487e, this.f483a);
        }

        /* renamed from: a */
        public void mo268a(int i) {
            mo269b().putInt(this.f487e, i);
            mo270c();
        }
    }

    /* renamed from: eu.chainfire.liveboot.d$c */
    public class C0152c {

        /* renamed from: c */
        protected C0149d f485c = null;

        /* renamed from: d */
        protected Editor f486d = null;

        /* renamed from: e */
        public String f487e = "";

        public C0152c(C0149d dVar, String str) {
            this.f485c = dVar;
            this.f487e = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public Editor mo269b() {
            if (this.f485c.f466b != null) {
                this.f486d = null;
                return this.f485c.f466b;
            }
            this.f486d = this.f485c.f465a.edit();
            return this.f486d;
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public void mo270c() {
            if (this.f485c.f466b == null && this.f486d != null) {
                this.f486d.commit();
                this.f486d = null;
            }
        }
    }

    /* renamed from: eu.chainfire.liveboot.d$d */
    public class C0153d extends C0152c {

        /* renamed from: a */
        public String f489a = "";

        public C0153d(C0149d dVar, String str, String str2) {
            super(dVar, str);
            this.f489a = str2;
        }

        /* renamed from: a */
        public String mo271a() {
            return this.f485c.f465a.getString(this.f487e, this.f489a);
        }

        /* renamed from: a */
        public void mo272a(String str) {
            mo269b().putString(this.f487e, str);
            mo270c();
        }
    }

    private C0149d(Context context) {
        this.f465a = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /* renamed from: a */
    public static C0149d m455a(Context context) {
        if (f464q == null) {
            f464q = new C0149d(context);
        }
        return f464q;
    }

    /* renamed from: a */
    public SharedPreferences mo264a() {
        return this.f465a;
    }
}
