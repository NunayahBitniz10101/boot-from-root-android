package p002eu.chainfire.liveboot;

import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.p005b.C0029a;

/* renamed from: eu.chainfire.liveboot.Application */
public class Application extends android.app.Application {
    public void onCreate() {
        super.onCreate();
        C0078d.m288a((String) "LiveBootApp");
        C0078d.m291a(false);
        C0029a.m103a(false);
    }
}
